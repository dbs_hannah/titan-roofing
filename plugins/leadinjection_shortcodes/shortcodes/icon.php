<?php

/*
    Icon
*/

add_shortcode('leadinjection_icon', 'leadinjection_icon_shortcode');

function leadinjection_icon_shortcode($atts)
{
    $default_atts = array(
        'icon_type' => 'fontawesome',
        'icon_fontawesome' => 'fa fa-adjust',
        'icon_iconssolid' => 'is is-icon-zynga',
        'icon_openiconic' => 'vc-oi vc-oi-dial',
        'icon_typicons' => 'typcn typcn-adjust-brightness',
        'icon_entypo' => 'entypo-icon entypo-icon-note',
        'icon_linecons' => 'vc_li vc_li-heart',
        'icon_image' => '',
        'icon_style' => '',
        'icon_outline' => '',
        'icon_size' => '',
        'alignment' => '',
        'icon_color' => 'icon-default',
        'icon_sign_color' => null,
        'icon_background_color' => null,
        'icon_border_color' => null,
        'linked' => false,
        'link_url' => '',
        'animation' => 'none',
        'css' => '',
        'xclass' => '',
        'shortcode_id' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('icon-', $shortcode_id);
    $wrapper_class = array($xclass, $responsive_helper, $alignment);

    $icon_class = array($icon_color, $icon_size, $icon_outline);

    // Enqueue needed icon font.
    vc_icon_element_fonts_enqueue( $icon_type );


    $style = array();

    $output_style = null;
    if('custom' == $icon_color) {

        $output_style  = '<style scoped>';
        $output_style .= '#'.$shortcode_id.' .li-icon-sign {color: '.$icon_sign_color.' !important; background-color: '.$icon_background_color.' !important; border-color: '.$icon_border_color.' !important;}';
        $output_style .= '#'.$shortcode_id.' .li-icon-sign a {color: '.$icon_sign_color.' !important;}';
        $output_style .= '</style>';

    }


    if (!is_null($icon_sign_color)) { $style[] = 'color: ' . $icon_sign_color . ';'; }

    if (!is_null($icon_background_color)) { $style[] = 'background-color: ' . $icon_background_color . ';';
    }

    if (!is_null($icon_border_color)) {
        $style[] = 'border-color: ' . $icon_border_color . ';';
    }

    $inline_style = '';
    if(count($style) !== 0)
    {
        $styles = implode(" ", $style);
        $inline_style = 'style="' . $styles . '"';
    }

    switch ($icon_type) {
        case 'fontawesome':
            $icon = '<i class="' . $icon_fontawesome . '"></i>';
            break;

        case 'iconssolid':
            $icon = '<span class="' . $icon_iconssolid . '"></span>';
            break;

        case 'openiconic':
            $icon = '<span class="oi ' . $icon_openiconic . '" aria-hidden="true"></span>';
            break;

        case 'typicons':
            $icon = '<span class="' . $icon_typicons . '"></span>';
            break;

        case 'entypo':
            $icon = '<span class="' . $icon_entypo . '"></span>';
            break;

        case 'linecons':
            $icon = '<span class="' . $icon_linecons . '"></span>';
            break;

        case 'image':
            $image_url = wp_get_attachment_image_src($icon_image);
            $icon = '<img src="' . $image_url[0] . '" alt="" />';
            $icon_style = 'image';
            break;
    }

    $icon_class[] = $icon_style;


    if('add_link' === $linked){
        $link = vc_build_link( $link_url );
        $icon_str = '<a href="' . $link['url'] . '">' . $icon . '</a>';
    }
    else{
        $icon_str = $icon;
    }

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');

    $icon_class = implode(' ', $icon_class);

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <?php echo $output_style; ?>

    <div class="li-icon <?php echo esc_attr($wrapper_class); ?>" id="<?php echo esc_attr($shortcode_id); ?>" <?php echo $data_effect; ?>>
        <div class="li-icon-sign <?php echo esc_attr($icon_class); ?>" <?php echo $inline_style; ?>>
            <?php echo $icon_str; ?>
        </div>
    </div>



    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_icon_vc');

function leadinjection_icon_vc()
{
    $leadinjection_icon_params = array(
        // Icon select fields
        leadinjection_icon_library_field(),
        leadinjection_icon_fontawsome_field(),
        leadinjection_icon_iconssolid_field(),
        leadinjection_icon_openiconic_field(),
        leadinjection_icon_typicons_field(),
        leadinjection_icon_entypo_field(),
        leadinjection_icon_linecons_field(),
        array(
            'type' => 'dropdown',
            'heading' => __('Select a icon style.', 'leadinjection'),
            'param_name' => 'icon_style',
            'admin_label' => true,
            'value' => array(
                __('Default', 'leadinjection') => '',
                __('Round', 'leadinjection') => 'round',
                __('Square', 'leadinjection') => 'square',
                __('Underline', 'leadinjection') => 'underline',
                __('Rounded', 'leadinjection') => 'square rounded',
                __('Zoom Big', 'leadinjection') => 'big',
            ),
            'dependency' => array(
                'element' => 'icon_type',
                'not_empty' => false,
                'value' => array('fontawesome', 'iconssolid', 'openiconic', 'typicons', 'entypo', 'linecons')
            ),
        ),
        array(
            'type' => 'checkbox',
            'param_name' => 'icon_outline',
            'value' => array(__('Display only the outline.', 'leadinjection') => 'outline'),
            'dependency' => array(
                'element' => 'icon_style',
                'not_empty' => false,
                'value' => array('round', 'square', 'square rounded')
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select a Icon size.', 'leadinjection'),
            'admin_label' => true,
            'param_name' => 'icon_size',
            'value' => array(
                __('Default', 'leadinjection') => 'none',
                __('Mini', 'leadinjection') => 'x05',
                __('Small', 'leadinjection') => 'x07',
                __('Large', 'leadinjection') => 'x12',
                __('Extra Large', 'leadinjection') => 'x15',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select button alignment.', 'leadinjection'),
            'param_name' => 'alignment',
            'value' => array(
                __('Default Alignment', 'leadinjection') => '',
                __('Inline', 'leadinjection') => 'inline',
                __('Left', 'leadinjection') => 'left',
                __('Center', 'leadinjection') => 'center',
                __('Rigth', 'leadinjection') => 'right',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select a Icon color.', 'leadinjection'),
            'admin_label' => true,
            'param_name' => 'icon_color',
            'value' => array(
                __('Theme Default', 'leadinjection') => 'icon-default',
                __('Red', 'leadinjection') => 'icon-red',
                __('Green', 'leadinjection') => 'icon-green',
                __('Blue', 'leadinjection') => 'icon-blue',
                __('Yellow', 'leadinjection') => 'icon-yellow',
                __('Gray', 'leadinjection') => 'icon-gray',
                __('Turquoise', 'leadinjection') => 'icon-turquoise',
                __('Purple', 'leadinjection') => 'icon-purple',
                __('White', 'leadinjection') => 'icon-white',
                __('Custom Color', 'leadinjection') => 'custom',
            ),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a icon color.', 'leadinjection'),
            'param_name' => 'icon_sign_color',
            'dependency' => array(
                'element' => 'icon_color',
                'not_empty' => false,
                'value' => array('custom')
            ),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a icon background color.', 'leadinjection'),
            'param_name' => 'icon_background_color',
            'dependency' => array(
                'element' => 'icon_color',
                'not_empty' => false,
                'value' => array('custom')
            ),
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a icon border color.', 'leadinjection'),
            'param_name' => 'icon_border_color',
            'dependency' => array(
                'element' => 'icon_outline',
                'not_empty' => false,
                'value' => array('outline')
            ),
        ),
        array(
            'type' => 'checkbox',
            'param_name' => 'linked',
            'value' => array(__('Add a link to the feature.', 'leadinjection') => 'add_link'),
        ),
        array(
            'type' => 'vc_link',
            'param_name' => 'link_url',
            'dependency' => array(
                'element' => 'linked',
                'not_empty' => false,
                'value' => array('add_link')
            ),
        ),
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),
    );

    $leadinjection_icon_params = leadinjection_add_responsive_helper_params($leadinjection_icon_params);

    vc_map(array(
            "name" => __("Icon", "leadinjection"),
            "base" => "leadinjection_icon",
            "icon" => 'li-icon li-single-icon',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Create an Icon', 'leadinjection'),
            "params" => $leadinjection_icon_params
        )
    );
}
