<?php
/**
*   Modal Shortcode
*/

add_shortcode('leadinjection_cf7_form', 'leadinjection_cf7_form_shortcode');

function leadinjection_cf7_form_shortcode($atts, $content)
{

$defaults = shortcode_atts(array(
'content' => !empty($content) ? $content : '',
'cf7_form_title' => '',
'xclass' => '',
), $atts);

extract($defaults);

    $args = array('post_type' => 'wpcf7_contact_form', 's' => $cf7_form_title);
    $query = get_posts( $args );



    var_dump($query);
    var_dump($cf7_form_title);

ob_start();

// Start Output
//////////////////////////////////////////////////////////////////////////////////////////
?>

    <?php //do_shortcode(); ?>

<?php
// End Output
//////////////////////////////////////////////////////////////////////////////////////////

$output = ob_get_contents();
ob_end_clean();

return $output;

}


/**
 * Visual Composer Registration
 */

add_action('vc_before_init', 'leadinjection_cf7_form_vc');

function leadinjection_cf7_form_vc()
{
    vc_map(array(
            "name" => __("Contactform 7", "leadinjection"),
            "base" => "leadinjection_cf7_form",
            "class" => "",
            //"icon" => 'li-icon li-modal',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Title and Sub Title for a Section', 'leadinjection'),
            "params" => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Available Modals', 'leadinjection'),
                    'value' => leadinjection_get_cf7_forms(),
                    'admin_label' => true,
                    'param_name' => 'cf7_form_title',
                    "std" => '',
                    'description' => __('Select icon library.', 'leadinjection'),
                ),
                leadinjection_xclass_field(),
            )
        )
    );
}