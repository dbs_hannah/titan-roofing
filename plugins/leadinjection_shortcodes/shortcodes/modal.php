<?php

/**
 *   Modal Shortcode
 */


add_shortcode('leadinjection_modal', 'leadinjection_modal_shortcode');

function leadinjection_modal_shortcode($atts, $content)
{
    $content = $modal_id = $exit_show = $time_show = $time_show_value = $scroll_show = $scroll_show_value = null;

    $defaults = shortcode_atts(array(
        'content' => !empty($content) ? $content : '',
        'modal_id' => null,
        'exit_show' => null,
        'time_show' => null,
        'time_show_value' => null,
        'scroll_show' => null,
        'scroll_show_value' => null,
        'xclass' => '',
    ), $atts);

    extract($defaults);

    $modal_data = get_post($modal_id);
    $modal_css = get_post_meta($modal_id, '_wpb_shortcodes_custom_css', true);
    $modal_dimensions = get_post_meta($modal_id, 'li_modal_dimensions');



    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>

    <style scoped> <?php echo $modal_css; ?>



        <?php //if('100%' == $modal_dimensions[0]['width'] && '100%' == $modal_dimensions[0]['height']) : ?>
        <?php
        /**
         * .modal{ width: <?php echo $modal_dimensions[0]['width']; ?>; height: <?php echo $modal_dimensions[0]['height']; ?>; padding: 0 !important;}
         * .li-modal .modal-dialog { margin: 0 auto; }
         */
        ?>
        <?php //endif; ?>

        <?php
        /** .modal-dialog{ width: <?php echo $modal_dimensions[0]['width']; ?>; height: <?php echo $modal_dimensions[0]['height']; ?>; }
         *  .li-modal .modal-dialog .modal-content{ width: <?php echo $modal_dimensions[0]['width']; ?>; height: <?php echo $modal_dimensions[0]['height']; ?>; }
         */
         ?>

        <?php if ( is_admin_bar_showing()) : ?>
        .li-modal .modal-dialog { margin: 32px auto 0 auto; }
        <?php endif; ?>

    </style>

    <?php if (!is_null($exit_show) || !is_null($time_show) || !is_null($scroll_show)) : ?>

    <script type="text/javascript">
        jQuery(window).load(function () {

            var exitpopped = 0;
            var showpop = 0;
            var delay = 3000;

            // Exit Popup
            <?php if(!is_null($exit_show)) :?>
            setTimeout(function () {
                showpop = 1;
            }, delay);
            jQuery(document).mousemove(function (e) {
                if (e.clientY <= 5 && exitpopped == 0 && showpop == 1) {
                    exitpopped = 1;
                    jQuery("#liModal-<?php echo $modal_id ?>").modal({show: true, backdrop: "static"});
                }
            });
            <?php endif; ?>


            // Show after time
            <?php if(!is_null($time_show)) :?>
            setTimeout(function () {
                jQuery("#liModal-<?php echo $modal_id ?>").modal({show: true, backdrop: "static"});
                exitpopped = 1;
            }, <?php echo (int)$time_show_value * 1000 ?>);
            <?php endif; ?>


            // Show after scroll xx %
            <?php if(!is_null($scroll_show)) :?>
            jQuery(window).scroll(function () {
                if (jQuery(window).scrollTop() >= (jQuery(document).height() - jQuery(window).height()) *<?php echo $scroll_show_value / 100; ?> && exitpopped == 0) {
                    exitpopped = 1;
                    jQuery("#liModal-<?php echo $modal_id ?>").modal({show: true, backdrop: "static"});
                }
            });
            <?php endif; ?>

        });
    </script>

<?php endif; ?>


    <div class="modal fade li-modal <?php echo esc_attr($xclass); ?>" id="liModal-<?php echo esc_attr($modal_id) ?>"
         tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <div class="modal-body">
                    <?php echo do_shortcode($modal_data->post_content); ?>
                </div>
            </div>
        </div>
    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/**
 * Visual Composer Registration
 */

add_action('vc_before_init', 'leadinjection_modal_vc');

function leadinjection_modal_vc()
{
    vc_map(array(
            "name" => __("Modal", "leadinjection"),
            "base" => "leadinjection_modal",
            "class" => "",
            "icon" => 'li-icon li-modal',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Title and Sub Title for a Section', 'leadinjection'),
            "params" => array(
                array(
                    'type' => 'dropdown',
                    'heading' => __('Available Modals', 'leadinjection'),
                    'value' => leadinjection_get_modals(),
                    'admin_label' => true,
                    'param_name' => 'modal_id',
                    'description' => __('Select icon library.', 'leadinjection'),
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'exit_show',
                    'value' => array(__('Display modal if the user exit', 'leadinjection') => 'yes'),
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'time_show',
                    'value' => array(__('Set a Modal timer', 'leadinjection') => 'yes'),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Show modal after time', 'leadinjection'),
                    'description' => __('Enter a time in sec.', 'leadinjection'),
                    'param_name' => 'time_show_value',
                    //'value' => '10',
                    'admin_label' => true,
                    'dependency' => array(
                        'element' => 'time_show',
                        'value' => 'yes',
                    ),
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'scroll_show',
                    'value' => array(__('Set scroll position', 'leadinjection') => 'yes'),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Show modal at scroll position', 'leadinjection'),
                    'description' => __('Enter a hight in procent.', 'leadinjection'),
                    'param_name' => 'scroll_show_value',
                    //'value' => '50',
                    'admin_label' => true,
                    'dependency' => array(
                        'element' => 'scroll_show',
                        'value' => 'yes',
                    ),
                ),
                leadinjection_xclass_field(),
            )
        )
    );
}