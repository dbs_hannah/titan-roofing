<?php

/*
    Video
*/

add_shortcode('leadinjection_video', 'leadinjection_video_shortcode');

function leadinjection_video_shortcode($atts, $content)
{
    $default_atts = array(
        'content' => !empty($content) ? $content : '',
        'embed_code' => '',
        'ratio' => 'embed-responsive-16by9',
        'alignment' => 'inline',
        'animation' => 'none',
        'css' => '',
        'shortcode_id' => '',
        'xclass' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('video-', $shortcode_id);
    $wrapper_class = array($xclass, $ratio, $alignment, $responsive_helper);


    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate ';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>

    <div id="<?php echo esc_attr($shortcode_id); ?>" class="lp-video embed-responsive <?php echo esc_attr($wrapper_class); ?>" <?php echo $data_effect; ?>>
        <iframe class="embed-responsive-item" src="<?php echo esc_url($embed_code); ?>"></iframe>
    </div>

     <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_video_vc');

function leadinjection_video_vc()
{
    $leadinjection_video_params = array(
        array(
            'type' => 'textarea',
            'heading' => __('Past your URL from the embed code', 'leadinjection'),
            'param_name' => 'embed_code',
            'value' => '',
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select a video ', 'leadinjection'),
            'param_name' => 'ratio',
            'value' => array(
                __('16:9 aspect ratio', 'leadinjection') => 'embed-responsive-16by9',
                __('4:3 aspect ratio', 'leadinjection') => 'embed-responsive-4by3',
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select Image alignment.', 'leadinjection'),
            'param_name' => 'alignment',
            'value' => array(
                __('Left', 'leadinjection') => 'left',
                __('Center', 'leadinjection') => 'center',
                __('Rigth', 'leadinjection') => 'right',
            ),
        ),
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),
    );

    $leadinjection_video_params = leadinjection_add_responsive_helper_params($leadinjection_video_params);

    vc_map(array(
            "name" => __("Video", "leadinjection"),
            "base" => "leadinjection_video",
            "class" => "",
            "icon" => 'li-icon li-embed-video',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Embed a Youtube/Vimeo Video', 'leadinjection'),
            "params" => $leadinjection_video_params
        )
    );
}

