<?php

/*
    Icon List
*/


add_shortcode('leadinjection_icon_list', 'leadinjection_icon_list_shortcode');

function leadinjection_icon_list_shortcode($atts, $content)
{

    $default_atts = array(
        'content' => !empty($content) ? $content : '',
        'animation' => 'none',
        'css' => '',
        'xclass' => '',
        'shortcode_id' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('ilist-', $shortcode_id);

    $wrapper_class = array($xclass, $responsive_helper);

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <ul class="list-unstyled li-icon-list <?php echo esc_attr($wrapper_class); ?>" id="<?php echo esc_attr($shortcode_id); ?>" <?php echo $data_effect; ?>>
        <?php echo do_shortcode($content); ?>
    </ul>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Image Review Slider Item
*/

add_shortcode('leadinjection_icon_list_item', 'leadinjection_icon_list_shortcode_item');

function leadinjection_icon_list_shortcode_item($atts, $content)
{
    $defaults = shortcode_atts(array(
        'content' => !empty($content) ? $content : '',
        'content_color' => '',
        'icon_type' => 'fontawesome',
        'icon_fontawesome' => 'fa fa-adjust',
        'icon_iconssolid' => 'is is-icon-zynga',
        'icon_openiconic' => 'vc-oi vc-oi-dial',
        'icon_typicons' => 'typcn typcn-adjust-brightness',
        'icon_entypo' => 'entypo-icon entypo-icon-note',
        'icon_linecons' => 'vc_li vc_li-heart',
        'icon_color' => '',
        'animation' => 'none',
        'xclass' => '',
    ), $atts);


    extract($defaults);

    $item_class = array($xclass);

    // TODO: Add Icon Type Custom Image

    // Enqueue needed icon font.
    vc_icon_element_fonts_enqueue($icon_type);

    $icon_color = ('' != $icon_color) ? 'style="color: ' . $icon_color . ';"' : null;
    $content_color = ('' != $content_color) ? 'style="color: ' . $content_color . ';"' : null;

    switch ($icon_type) {
        case 'fontawesome':
            $icon = '<i class="' . $icon_fontawesome . '" ' . $icon_color . '></i>';
            break;

        case 'openiconic':
            $icon = '<span class="oi ' . $icon_openiconic . '" aria-hidden="true" ' . $icon_color . '></span>';
            break;

        case 'typicons':
            $icon = '<span class="' . $icon_typicons . '" ' . $icon_color . '></span>';
            break;

        case 'entypo':
            $icon = '<span class="' . $icon_entypo . '" ' . $icon_color . '></span>';
            break;

        case 'linecons':
            $icon = '<span class="' . $icon_linecons . '" ' . $icon_color . '></span>';
            break;

        case 'iconssolid':
            $icon = '<span class="' . $icon_iconssolid . '" ' . $icon_color . '></span>';
            break;

    }

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $item_class[] = 'li-animate';
        $data_effect = 'data-effect="' . $animation . '"';
    }

    $item_class = implode(' ', $item_class);


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <li class="<?php echo $item_class; ?>" <?php echo $content_color; ?> <?php echo $data_effect; ?>>
        <?php echo $icon; ?> <?php echo $content; ?>
    </li>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_icon_list_vc');

function leadinjection_icon_list_vc()
{

    $leadinjection_icon_list_params = array(
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),
    );

    $leadinjection_icon_list_params = leadinjection_add_responsive_helper_params($leadinjection_icon_list_params);

    vc_map(array(
            "name" => __("Icon List", "leadinjection"),
            "base" => "leadinjection_icon_list",
            "as_parent" => array('only' => 'leadinjection_icon_list_item'),
            "content_element" => true,
            "show_settings_on_create" => false,
            "is_container" => true,
            "class" => '',
            "icon" => 'li-icon li-icon-list',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('List with icons on the left.', 'leadinjection'),
            "params" => $leadinjection_icon_list_params,
            "js_view" => 'VcColumnView'
        )
    );

    vc_map(array(
            "name" => __("Icon List Item", "leadinjection"),
            "base" => "leadinjection_icon_list_item",
            "icon" => 'li-icon li-icon-list',
            "content_element" => true,
            "as_child" => array('only' => 'leadinjection_icon_list'),
            "params" => array(
                // Icon select fields
                leadinjection_icon_library_field(),
                leadinjection_icon_fontawsome_field(),
                leadinjection_icon_iconssolid_field(),
                leadinjection_icon_openiconic_field(),
                leadinjection_icon_typicons_field(),
                leadinjection_icon_entypo_field(),
                leadinjection_icon_linecons_field(),
                //leadinjection_icon_image_field(),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a icon color', 'leadinjection'),
                    'param_name' => 'icon_color',
                ),
                array(
                    'type' => 'textarea',
                    'heading' => __('Enter Item Content', 'leadinjection'),
                    'param_name' => 'content',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select content color.', 'leadinjection'),
                    'param_name' => 'content_color',
                ),
                array(
                    'type' => 'animation_style',
                    'heading' => __('Animation', 'leadinjection'),
                    'param_name' => 'animation',
                ),
                leadinjection_xclass_field(),
            )
        )
    );
}


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_leadinjection_icon_list extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_leadinjection_icon_list_item extends WPBakeryShortCode
    {
    }
}