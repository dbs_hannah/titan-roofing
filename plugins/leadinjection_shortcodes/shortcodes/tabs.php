<?php

/*
    Tabs
*/


add_shortcode('leadinjection_tabs', 'leadinjection_tabs_shortcode');

function leadinjection_tabs_shortcode($atts, $content)
{
    if( isset( $GLOBALS['tabs_count'] ) ) {
        $GLOBALS['tabs_count']++;
    }else {
        $GLOBALS['tabs_count'] = 0;
    }
    $GLOBALS['tabs_default_count'] = 0;


    $defaults = shortcode_atts(array(
        'content' => !empty($content) ? $content : '',
        'animation' => 'none',
        'css' => '',
    ), $atts);

    $id = 'custom-tabs-'. $GLOBALS['tabs_count'];

    $atts_map = leadinjection_attribute_map( $content );

    if ( $atts_map ) {
        $tabs = array();
        $GLOBALS['tabs_default_active'] = true;
        foreach( $atts_map as $check ) {
            if( !empty($check["tab"]["active"]) ) {
                $GLOBALS['tabs_default_active'] = false;
            }
        }
        $i = 0;
        foreach( $atts_map as $tab ) {

            $tab_href='custom-tab-' . $GLOBALS['tabs_count'] . '-' . md5($tab["leadinjection_tabs_item"]["title"]);
            $is_active = (isset($tab['leadinjection_tabs_item']['active'])) ? 'active' : null;

            $tabs[] = ' <li role="presentation" class="'.$is_active.'"><a href="#'.$tab_href.'" role="tab" data-toggle="tab">'.$tab['leadinjection_tabs_item']['title'].'</a></li>';
            $i++;
        }
    }


    extract($defaults);

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div class="li-tabs">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <?php echo ($tabs) ? implode( $tabs ) : ''; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <?php echo do_shortcode($content) ?>
        </div>

    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Image Review Slider Item
*/

add_shortcode('leadinjection_tabs_item', 'leadinjection_tabs_shortcode_item');

function leadinjection_tabs_shortcode_item($atts, $content)
{

    $defaults = shortcode_atts(array(
        'title' => '',
        'content' => !empty($content) ? $content : '',
        'active' => '',
    ), $atts);

    if( $GLOBALS['tabs_default_active'] && $GLOBALS['tabs_default_count'] == 0 ) {
        $atts['active'] = true;
    }
    $GLOBALS['tabs_default_count']++;

    $id = 'custom-tab-'. $GLOBALS['tabs_count'] . '-'. md5( $atts['title'] );

    extract($defaults);

    $is_active = ('' !== $active) ? 'active' : 'fade';

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div role="tabpanel" class="tab-pane <?php echo $is_active; ?>" id="<?php echo $id; ?>">
        <?php echo do_shortcode($content); ?>
    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_tabs_vc');

function leadinjection_tabs_vc()
{
    vc_map(array(
            "name" => __("Tabs", "leadinjection"),
            "base" => "leadinjection_tabs",
            "as_parent" => array('only' => 'leadinjection_tabs_item'),
            "content_element" => true,
            "show_settings_on_create" => false,
            "is_container" => true,
            "class" => '',
            "icon" => 'li-icon li-tabs',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Create tabbed content', 'leadinjection'),
            "params" => array(
                leadinjection_animation_field(),
                leadinjection_css_editor_field(),
            ),
            "js_view" => 'VcColumnView'
        )
    );

    vc_map(array(
            "name" => __("Tabs Item", "leadinjection"),
            "base" => "leadinjection_tabs_item",
            "icon" => 'li-icon li-tabs',
            "content_element" => true,
            "as_child" => array('only' => 'leadinjection_tabs'),
            "params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter a tab name.', 'leadinjection'),
                    'param_name' => 'title',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textarea_html',
                    'heading' => __('Enter a tab content.', 'leadinjection'),
                    'param_name' => 'content',
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'active',
                    'value' => array(__('Make this tab active.', 'leadinjection') => 'active'),
                ),
            )
        )
    );
}


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_leadinjection_tabs extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_leadinjection_tabs_item extends WPBakeryShortCode
    {
    }
}