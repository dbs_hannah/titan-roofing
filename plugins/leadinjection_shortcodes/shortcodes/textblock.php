<?php

/*
    Textblock
*/

add_shortcode('leadinjection_textblock', 'leadinjection_textblock_shortcode');

function leadinjection_textblock_shortcode($atts, $content)
{

    $default_atts = array(
        'content' => !empty($content) ? $content : '',
        'content_color' => '',
        'animation' => 'none',
        'css' => '',
        'shortcode_id' => '',
        'xclass' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('tb-', $shortcode_id);
    $wrapper_class = array($xclass, $responsive_helper);

    $content_color = ('' !== $content_color) ? 'style="color: '.$content_color.';"' : null;

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate ';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div id="<?php echo esc_attr($shortcode_id); ?>" class="li-textblock <?php echo esc_attr($wrapper_class); ?>" <?php echo $content_color; ?> <?php echo $data_effect; ?>>
        <?php echo wpb_js_remove_wpautop( $content, true ); ?>
    </div>


     <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_textblock_vc');

function leadinjection_textblock_vc()
{
    $leadinjection_textblock_params = array(
        array(
            'type' => 'textarea_html',
            'heading' => __('Textblock', 'leadinjection'),
            'param_name' => 'content',
            'admin_label' => true,
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a Textblock Color', 'leadinjection'),
            'param_name' => 'content_color',
        ),
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),
    );

    $leadinjection_textblock_params = leadinjection_add_responsive_helper_params($leadinjection_textblock_params);

    vc_map(array(
            "name" => __("Textblock", "leadinjection"),
            "base" => "leadinjection_textblock",
            "class" => "",
            "icon" => 'li-icon li-textblock',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('A simple textblock', 'leadinjection'),
            "params" => $leadinjection_textblock_params
        )
    );
}

