<?php

/*
    Side Icon Text
*/

add_shortcode('leadinjection_side_icon_text', 'leadinjection_side_icon_text_shortcode');

function leadinjection_side_icon_text_shortcode($atts, $content)
{
    $default_atts = array(
        'icon_type' => 'fontawesome',
        'icon_fontawesome' => 'fa fa-adjust',
        'icon_iconssolid' => 'is is-icon-zynga',
        'icon_openiconic' => 'vc-oi vc-oi-dial',
        'icon_typicons' => 'typcn typcn-adjust-brightness',
        'icon_entypo' => 'entypo-icon entypo-icon-note',
        'icon_linecons' => 'vc_li vc_li-heart',
        'icon_color' => null,
        'title' => '',
        'title_color' => null,
        'content' => !empty($content) ? $content : '',
        'content_color' => null,
        'align' => 'left',
        'animation' => 'none',
        'css' => '',
        'shortcode_id' => '',
        'xclass' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('sit-', $shortcode_id);
    $wrapper_class = array($xclass, $align, $responsive_helper);


    // Enqueue needed icon font.
    vc_icon_element_fonts_enqueue( $icon_type );

    if (!is_null($icon_color)) {
        $icon_color = 'style="color: ' . esc_attr($icon_color) . ';"';
    }

    if (!is_null($title_color)) {
        $title_color = 'style="color: ' . esc_attr($title_color) . ';"';
    }

    if (!is_null($content_color)) {
        $content_color = 'style="color: ' . esc_attr($content_color) . ';"';
    }

    switch ($icon_type) {
        case 'fontawesome':
            $icon = '<i class="' . esc_attr($icon_fontawesome) . '"></i>';
            break;

        case 'openiconic':
            $icon = '<span class="oi ' . esc_attr($icon_openiconic) . '" aria-hidden="true"></span>';
            break;

        case 'typicons':
            $icon = '<span class="' . esc_attr($icon_typicons) . '"></span>';
            break;

        case 'entypo':
            $icon = '<span class="' . esc_attr($icon_entypo) . '"></span>';
            break;

        case 'linecons':
            $icon = '<span class="' . esc_attr($icon_linecons) . '"></span>';
            break;

        case 'iconssolid':
            $icon = '<span class="' . esc_attr($icon_iconssolid) . '"></span>';
            break;
    }

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate ';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div class="li-side-icon-text <?php echo esc_attr($wrapper_class); ?>" <?php echo $data_effect; ?>>
        <div class="li-side-icon-text-icon" <?php echo $icon_color; ?>><?php echo $icon; ?></div>
        <h3 class="li-side-icon-text-title" <?php echo $title_color; ?>><?php echo esc_html($title); ?></h3>
        <div class="li-side-icon-text-content" <?php echo $content_color; ?>><?php echo $content; ?></div>
    </div>



    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();


    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_side_icon_text_vc');

function leadinjection_side_icon_text_vc()
{
    $leadinjection_side_icon_text_params = array(
        // Icon select fields
        leadinjection_icon_library_field(),
        leadinjection_icon_fontawsome_field(),
        leadinjection_icon_iconssolid_field(),
        leadinjection_icon_openiconic_field(),
        leadinjection_icon_typicons_field(),
        leadinjection_icon_entypo_field(),
        leadinjection_icon_linecons_field(),
        //leadinjection_icon_image_field(),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select an Icon color', 'leadinjection'),
            'param_name' => 'icon_color',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Enter title here', 'leadinjection'),
            'param_name' => 'title',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a title color', 'leadinjection'),
            'param_name' => 'title_color',
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Enter content here', 'leadinjection'),
            'param_name' => 'content',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a content color', 'leadinjection'),
            'param_name' => 'content_color',
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Select a content alignment', 'leadinjection'),
            'param_name' => 'align',
            'value' => array(
                __('Align left', 'leadinjection') => 'left',
                __('Align right', 'leadinjection') => 'right',),
        ),
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),
    );

    $leadinjection_side_icon_text_params = leadinjection_add_responsive_helper_params($leadinjection_side_icon_text_params);

    vc_map(array(
            "name" => __("Side Icon Text", "leadinjection"),
            "base" => "leadinjection_side_icon_text",
            "class" => "",
            "icon" => 'li-icon li-side-icon',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Text with an Icon (left or right)', 'leadinjection'),
            "params" => $leadinjection_side_icon_text_params
        )
    );
}

