<?php

/*
    Textblock
*/

add_shortcode('leadinjection_location_map', 'leadinjection_location_map_shortcode');

function leadinjection_location_map_shortcode($atts, $content)
{

    $default_atts = array(
        'content' => !empty($content) ? $content : '',
        'address_title' => '',
        'street' => '',
        'city' => '',
        'state' => '',
        'zip' => '',
        'country' => '',
        'address_box_color' => '',
        'enable_button' => '',
        'btn_color' => '',
        'btn_background_color' => '',
        'btn_border_color' => '',
        'btn_value_color' => '',
        'animation' => 'none',
        'css' => '',
        'shortcode_id' => '',
        'xclass' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('map-', $shortcode_id);
    $wrapper_class = array($xclass, $responsive_helper);

    $box_color = $border_color = '';

    if('' !== $address_box_color){
        $box_color = 'style="background-color: '.$address_box_color.';"';
        $border_color = 'style="border-bottom-color: '.$address_box_color.';"';
    }

    $output_style = null;
    if ('custom' === $btn_color) {

        $bacground_css = ('' !== $btn_background_color) ? 'background-color: ' . $btn_background_color . '; ' : null;
        $border_css = ('' !== $btn_border_color) ? 'border-color: ' . $btn_border_color . '; ' : null;
        $value_css = ('' !== $btn_value_color) ? 'color:' . $btn_value_color . '; ' : null;


        $output_style = '<style scoped>';
        $output_style .= '#' . $shortcode_id . ' .btn{' . $bacground_css . $border_css . $value_css . '}';
        $output_style .= '</style>';
    }

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate ';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');

    // leadinjection Global options
    $leadinjection_global_option = get_option( 'rdx_option' );

    if(!empty($leadinjection_global_option['li-global-api-key-gmaps'])){
        wp_enqueue_script('li-google-maps', 'https://maps.googleapis.com/maps/api/js?key='.$leadinjection_global_option['li-global-api-key-gmaps']);
    }else{
        $api_notice = true;
    }

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>

    <?php echo $output_style; ?>

    <script>

        <?php if(isset($api_notice)) :?>
            alert('To use the Location Map you need to set your Google Map API Key in the Leadinjection API Keys! For more information go to Leadinjection Options > API Keys')
        <?php endif; ?>

        jQuery(document).ready(function($) {

            var companyName = '<?php echo  esc_html( $address_title ); ?>';
            var address = '<?php echo esc_html( $street ) ?> <?php echo esc_html( $city ) ?> <?php echo esc_html( $state ) ?> <?php echo esc_html( $zip ) ?>';

            if ($('#location-map-gmap').length) {
                var geocoder;
                var map;

                var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
                var draggable = w > 992 ? true : false; // Disable on Mobile


                geocoder = new google.maps.Geocoder();

                var mapOptions = {
                    zoom: 14,
                    scrollwheel: false,
                    draggable: draggable,
                    mapTypeControl: false,
                    center: new google.maps.LatLng(0, 0),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map = new google.maps.Map(document.getElementById('location-map-gmap'), mapOptions);

                var contentString = '<div id="content">' +
                    '<strong>' + companyName + '</strong><br>' +
                    'Address: ' + address +
                    '</div>';

                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                geocoder.geocode({'address': address}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            icon: '<?php echo get_template_directory_uri(); ?>/img/mapmarker.png',
                            title: address
                        });

                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.open(map, marker);
                        });

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
            });
    </script>

    <div id="<?php echo esc_attr($shortcode_id); ?>" class="location-map <?php echo $wrapper_class; ?>" >
        <div id="location-map-gmap" <?php echo $border_color; ?>></div>
        <div class="location-map-address">
            <div class="location-map-address-box" <?php echo $box_color; ?>>
                <i class="glyphicon glyphicon-map-marker title-marker"></i>
                <address>
                    <?php echo esc_html($street); ?> <br />
                    <?php echo esc_html($city); ?>, <?php echo esc_html($state); ?> <?php echo esc_html($zip); ?> <br />
                    <?php echo esc_html($country); ?> <br />
                </address>
                <?php if(!empty($enable_button)) : ?>
                <a href="https://maps.google.com?daddr=<?php echo esc_html(urlencode($street)); ?>+<?php echo esc_html(urlencode($state)); ?>+<?php echo esc_html(urlencode($zip)); ?>+<?php echo esc_html(urlencode($country)); ?>" class="btn <?php echo $btn_color; ?> btn-md btn-icon-right">Get Direction <i class="fa fa-fw fa-arrow-right"></i></a>
                <?php endif; ?>
            </div>
        </div>
    </div>


     <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_location_map_vc');

function leadinjection_location_map_vc()
{
    vc_map(array(
            "name" => __("Location Map", "leadinjection"),
            "base" => "leadinjection_location_map",
            "class" => "",
            "icon" => 'li-icon li-location-map',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Location Map with Direction Button', 'leadinjection'),
            "params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter a Marker Title', 'leadinjection'),
                    'param_name' => 'address_title',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Street, Address', 'leadinjection'),
                    'param_name' => 'street',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('City', 'leadinjection'),
                    'param_name' => 'city',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('State', 'leadinjection'),
                    'param_name' => 'state',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('ZIP Code', 'leadinjection'),
                    'param_name' => 'zip',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Country', 'leadinjection'),
                    'param_name' => 'country',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Address Block Color', 'leadinjection'),
                    'description' => __('Select a Address Block Color', 'leadinjection'),
                    'param_name' => 'address_box_color',
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'enable_button',
                    'value' => array(__('Add an direction button.', 'leadinjection') => 'yes'),
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Select a button color.', 'leadinjection'),
                    'admin_label' => true,
                    'param_name' => 'btn_color',
                    'value' => array(
                        __('Defualt', 'leadinjection') => '',
                        __('Red', 'leadinjection') => 'btn-red',
                        __('Green', 'leadinjection') => 'btn-green',
                        __('Blue', 'leadinjection') => 'btn-blue',
                        __('Yellow', 'leadinjection') => 'btn-yellow',
                        __('Gray', 'leadinjection') => 'btn-gray',
                        __('Turquoise', 'leadinjection') => 'btn-turquoise',
                        __('Purple', 'leadinjection') => 'btn-purple',
                        __('White', 'leadinjection') => 'btn-white',
                        __('Custom Style 1', 'leadinjection') => 'btn-custom1',
                        __('Custom Style 2', 'leadinjection') => 'btn-custom2',
                        __('Custom Style 3', 'leadinjection') => 'btn-custom3',
                        __('Custom Style 4', 'leadinjection') => 'btn-custom4',
                        __('Custom Color', 'leadinjection') => 'custom',
                    ),
                    'dependency' => array(
                        'element' => 'enable_button',
                        'value' => 'yes',
                    ),
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a button background color.', 'leadinjection'),
                    'param_name' => 'btn_background_color',
                    'dependency' => array(
                        'element' => 'btn_color',
                        'value' => 'custom',
                    ),
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a button border color.', 'leadinjection'),
                    'param_name' => 'btn_border_color',
                    'dependency' => array(
                        'element' => 'btn_color',
                        'value' => 'custom',
                    ),
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a button text color.', 'leadinjection'),
                    'param_name' => 'btn_value_color',
                    'dependency' => array(
                        'element' => 'btn_color',
                        'value' => 'custom',
                    ),
                ),
                leadinjection_animation_field(),
                leadinjection_css_editor_field(),
                leadinjection_xclass_field(),
                leadinjection_shortcode_id_field(),
            )
        )
    );
}

