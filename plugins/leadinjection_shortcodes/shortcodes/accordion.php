<?php

/*
    Accordion
*/


add_shortcode('leadinjection_accordion', 'leadinjection_accordion_shortcode');

function leadinjection_accordion_shortcode($atts, $content)
{
    if( isset($GLOBALS['accordion_count']) ) {
        $GLOBALS['accordion_count']++;
    }else {
        $GLOBALS['accordion_count'] = 0;
    }
    $accourdion_id = $GLOBALS['accordion_count'];

 
    $default_atts = array(
        'content' => !empty($content) ? $content : '',
        'animation' => 'none',
        'css' => '',
        'xclass' => '',
    );

  
    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);


    $wrapper_class = array($xclass, $responsive_helper);

    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div class="panel-group li-accordion <?php echo esc_attr($wrapper_class); ?>" id="accordion-<?php echo esc_attr($accourdion_id) ?>" <?php echo $data_effect; ?> role="tablist" aria-multiselectable="true">
        <?php echo do_shortcode($content); ?>
    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Image Review Slider Item
*/

add_shortcode('leadinjection_accordion_item', 'leadinjection_accordion_shortcode_item');

function leadinjection_accordion_shortcode_item($atts, $content)
{
    if( isset($GLOBALS['collapse_count']) ) {
        $GLOBALS['collapse_count']++;
    }else{
        $GLOBALS['collapse_count'] = 0;
    }
    $collapse_id = $GLOBALS['collapse_count'];
    $accourdion_id = $GLOBALS['accordion_count'];

    $defaults = shortcode_atts(array(
        'item_color' => '',
        'title' => '',
        'content' => !empty($content) ? $content : '',
        'open' => '',
        'xclass' => '',
    ), $atts);


    extract($defaults);

    $color = '';
    if('' !== $item_color){
        $color = 'style="background-color: '.$item_color.'; border-color: '.$item_color.';"';
    }


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>


    <div class="panel panel-default <?php echo esc_attr($xclass); ?>" <?php echo $color; ?>>
        <div class="panel-heading" role="tab" id="heading-<?php echo esc_attr($collapse_id); ?>" <?php echo $color; ?>>
            <h4 class="panel-title">
                <a role="button" data-toggle="collapse" data-parent="#accordion-<?php echo esc_attr($accourdion_id); ?>"
                   href="#collapse-<?php echo esc_attr($collapse_id); ?>" aria-expanded="true" aria-controls="collapse-<?php echo esc_attr($collapse_id); ?>">
                    <?php echo esc_html($title); ?>
                    <?php if('in' === $open) : ?>
                        <i class="fa fa-dot-circle-o"></i>
                    <?php else : ?>
                        <i class="fa fa-circle-o"></i>
                    <?php endif; ?>
                </a>
            </h4>
        </div>
        <div id="collapse-<?php echo esc_attr($collapse_id); ?>" class="panel-collapse collapse <?php echo esc_attr($open); ?>" role="tabpanel" aria-labelledby="heading-<?php echo esc_attr($collapse_id); ?>">
            <div class="panel-body">
                <?php echo do_shortcode($content); ?>
            </div>
        </div>
    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_accordion_vc');

function leadinjection_accordion_vc()
{

    $leadinjection_accordion_params = array(
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
    );

    $leadinjection_accordion_params = leadinjection_add_responsive_helper_params($leadinjection_accordion_params);

    vc_map(array(
            "name" => __("Accordion", "leadinjection"),
            "base" => "leadinjection_accordion",
            "as_parent" => array('only' => 'leadinjection_accordion_item'),
            "content_element" => true,
            "show_settings_on_create" => true,
            "is_container" => true,
            "class" => '',
            "icon" => 'li-icon li-accordion',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Collapsible content panels', 'leadinjection'),
            "params" => $leadinjection_accordion_params,
            "js_view" => 'VcColumnView'
        )
    );

    vc_map(array(
            "name" => __("Accordion Item", "leadinjection"),
            "base" => "leadinjection_accordion_item",
            "icon" => 'li-icon li-accordion',
            "content_element" => true,
            "as_child" => array('only' => 'leadinjection_accordion'),
            "params" => array(
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a Accordion Item color.', 'leadinjection'),
                    'param_name' => 'item_color',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter a collapse title.', 'leadinjection'),
                    'param_name' => 'title',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textarea_html',
                    'heading' => __('Enter a collapse content.', 'leadinjection'),
                    'param_name' => 'content',
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'open',
                    'value' => array(__('Display this collapes open.', 'leadinjection') => 'in'),
                ),
                leadinjection_xclass_field(),
            )
        )
    );
}


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_leadinjection_accordion extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_leadinjection_accordion_item extends WPBakeryShortCode
    {
    }
}