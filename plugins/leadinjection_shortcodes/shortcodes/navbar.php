<?php

/*
    Navbar
*/


add_shortcode('leadinjection_navbar', 'leadinjection_navbar_shortcode');

function leadinjection_navbar_shortcode($atts, $content)
{
    $defaults = shortcode_atts(array(
        'content' => !empty($content) ? $content : '',
        'nav_display' => '',
        'nav_width' => 'container',
        'brand_name' => '',
        'brand_logo' => '',
        'background_color' => '',
        'link_color' => '',
        'xclass' => '',
        'shortcode_id' => '',
    ), $atts);

    extract($defaults);
    $shortcode_id = leadinjection_custom_id('navbar-', $shortcode_id);
    $wrapper_class = $xclass.' '.$nav_display;

    if ('' !== $brand_logo) {
        $image_url = wp_get_attachment_image_src($brand_logo, 'full');
        $brand = '<a class="navbar-brand" href="#">';
        $brand .= '<img src="' . esc_url($image_url[0]) . '" alt="' . esc_attr($brand_name) . '" />';
        $brand .= '</a>';
    } else {
        $brand = '<a class="navbar-brand" href="#">' . $brand_name . '</a>';
    }

    $navbar_background = ('' !== $background_color) ? 'style="background-color: ' . $background_color . ';"' : null;

    $output_style = '';
    if ('' !== $link_color) {
        $output_style  = '<style>';
        $output_style .= '#'.$shortcode_id.' .nav li.active a { background-color: transparent; color: '.$link_color.'; }';
        $output_style .= '#'.$shortcode_id.' .nav li a:hover { background-color: transparent; color: '.$link_color.'; }';
        $output_style .= '#'.$shortcode_id.' .nav li.active a:hover { background-color: transparent; color: '.$link_color.'; }';
        $output_style .= '#'.$shortcode_id.' .nav li.active a:focus { background-color: transparent; color: '.$link_color.'; }';
        $output_style .= '</style>';
    }

    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>

    <nav id="<?php echo esc_attr($shortcode_id); ?>" class="navbar navbar-default li-navbar <?php echo esc_attr($wrapper_class); ?>" <?php echo $navbar_background; ?>>
        <div class="<?php echo $nav_width; ?>">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-<?php echo esc_attr($shortcode_id); ?>" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                </button>
                <div class="navbar-brand-container">
                    <?php echo $brand; ?>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="collapse-<?php echo $shortcode_id; ?>">
                <ul class="nav navbar-nav navbar-right">
                    <?php echo do_shortcode($content); ?>
                </ul>
            </div>
        </div>
    </nav>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();

    return $output_style.$output;

}


/*
    Image Review Slider Item
*/

add_shortcode('leadinjection_navbar_item', 'leadinjection_navbar_shortcode_item');

function leadinjection_navbar_shortcode_item($atts, $content)
{
    $defaults = shortcode_atts(array(
        'content' => !empty($content) ? $content : '',
        'is_active' => '',
        'section_id' => '',
        'xclass' => '',
        'shortcode_id' => '',
    ), $atts);


    extract($defaults);
    $shortcode_id = leadinjection_custom_id('navbar-item', $shortcode_id);

    $classes = array($xclass);
    $classes[] = ('' !== $is_active) ? 'active' : null;

    $li_class = implode(' ', $classes);

    // Create Output
    $output = '<li class="' . esc_attr($li_class) . '" id="'.esc_attr($shortcode_id).'"><a class="scroll-to" href="' . $section_id . '">' . $content . '</a></li>';

    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_navbar_vc');

function leadinjection_navbar_vc()
{
    vc_map(array(
            "name" => __("Navbar", "leadinjection"),
            "base" => "leadinjection_navbar",
            "as_parent" => array('only' => 'leadinjection_navbar_item'),
            "content_element" => true,
            "show_settings_on_create" => true,
            "is_container" => true,
            "class" => '',
            "icon" => 'li-icon li-onpage-nav',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Create onpage navigation', 'leadinjection'),
            "params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter a brand name', 'leadinjection'),
                    'param_name' => 'brand_name',
                    'value' => 'Brand Name',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'attach_image',
                    'heading' => __('Select a Navbar logo', 'leadinjection'),
                    'param_name' => 'brand_logo',
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'nav_width',
                    'value' => array(__('Display Navigation full width', 'leadinjection') => 'container-fluid'),
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a Navbar background color.', 'leadinjection'),
                    'param_name' => 'background_color',
                ),
                array(
                    'type' => 'colorpicker',
                    'heading' => __('Select a Navbar link color.', 'leadinjection'),
                    'param_name' => 'link_color',
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => __('Select a display option.', 'leadinjection'),
                    'value' => array(
                        __('Display Top', 'leadinjection') => '',
                        __('Display Fix Top', 'leadinjection') => 'navbar-fixed-top',
                        __('Fade-In Fix Top', 'leadinjection') => 'navbar-fixed-top navbar-hidden',
                    ),
                    'param_name' => 'nav_display',
                ),
                leadinjection_xclass_field(),
                leadinjection_shortcode_id_field(),
            ),
            "js_view" => 'VcColumnView'
        )
    );

    vc_map(array(
            "name" => __("Navbar Item", "leadinjection"),
            "base" => "leadinjection_navbar_item",
            "content_element" => true,
            "icon" => 'li-icon li-onpage-nav',
            "as_child" => array('only' => 'leadinjection_navbar'),
            "params" => array(
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter a Nav-Item content', 'leadinjection'),
                    'param_name' => 'content',
                    'value' => 'Nav-Item Content',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'textfield',
                    'heading' => __('Enter Section ID', 'leadinjection'),
                    'param_name' => 'section_id',
                    'value' => '#section-id',
                    'admin_label' => true,
                ),
                array(
                    'type' => 'checkbox',
                    'param_name' => 'is_active',
                    'value' => array(__('Display this atem active.', 'leadinjection') => 'active'),
                ),
                leadinjection_xclass_field(),
                leadinjection_shortcode_id_field(),
            )
        )
    );
}


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if (class_exists('WPBakeryShortCodesContainer')) {
    class WPBakeryShortCode_leadinjection_navbar extends WPBakeryShortCodesContainer
    {
    }
}
if (class_exists('WPBakeryShortCode')) {
    class WPBakeryShortCode_leadinjection_navbar_item extends WPBakeryShortCode
    {
    }
}