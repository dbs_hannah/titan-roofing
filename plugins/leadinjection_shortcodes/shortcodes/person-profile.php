<?php

/*
    Person Profile
*/

add_shortcode('leadinjection_person_profile', 'leadinjection_person_profile_shortcode');

function leadinjection_person_profile_shortcode($atts, $content)
{

    $default_atts = array(
        'person_image' => '',
        'person_name' => '',
        'person_name_color' => '',
        'person_title' => '',
        'person_title_color' => '',
        'person_desc' => '',
        'person_desc_color' => '',
        'content' => !empty($content) ? $content : '',
        'css' => '',
        'animation' => 'none',
        'social_links_color' => '',
        'facebook_url' => '',
        'twitter_url' => '',
        'google_plus_url' => '',
        'email_address' => '',
        'linkedin_url' => '',
        'xclass' => '',
        'shortcode_id' => '',
    );

    $default_atts = leadinection_add_responsive_helper_atts($default_atts);
    $defaults = shortcode_atts($default_atts, $atts);
    $responsive_helper =  leadinjection_create_responsive_helper_classes($defaults);

    extract($defaults);

    $shortcode_id = leadinjection_custom_id('pp-', $shortcode_id);
    $wrapper_class = array($xclass, $responsive_helper);

    $default_image = array(get_template_directory_uri() . '/img/person-profile-default.png');
    $person_image = ('' != $person_image) ? wp_get_attachment_image_src($person_image, 'full') : $default_image;


    $data_effect = '';
    if ('none' !== $animation) {
        leadinjection_enqueue_animation();
        $wrapper_class[] = 'li-animate';
        $data_effect = 'data-effect="' . esc_attr($animation) . '"';
    }

    $output_style = '';
    if ('' != $social_links_color) {
        $output_style = '<style scoped>';
        $output_style .= '#'.$shortcode_id.' .person-profile-social-links li a{ background-color: ' . $social_links_color . '; }';
        $output_style .= '#'.$shortcode_id.' .person-profile-social-links li a:hover{ background-color: #fff; border-color: ' . $social_links_color . '; color: ' . $social_links_color . '; }';
        $output_style .= '</style>';
    }

    $social_icons  = ('' != $facebook_url) ? '<li><a href="' . esc_url($facebook_url) . '"><i class="fa fa-fw fa-facebook"></i></a></li>' : null;
    $social_icons .= ('' != $twitter_url) ? '<li><a href="' . esc_url($twitter_url) . '"><i class="fa fa-fw fa-twitter"></i></a></li>' : null;
    $social_icons .= ('' != $google_plus_url) ? '<li><a href="' . esc_url($google_plus_url) . '"><i class="fa fa-fw fa-google-plus"></i></a></li>' : null;
    $social_icons .= ('' != $email_address) ? '<li><a href="mailto:' . $email_address . '"><i class="fa fa-fw fa-at"></i></a></li>' : null;
    $social_icons .= ('' != $linkedin_url) ? '<li><a href="' . esc_url($linkedin_url) . '"><i class="fa fa-fw fa-linkedin"></i></a></li>' : null;

    $person_name_color = ('' != $person_name_color) ? 'style="color: ' . $person_name_color . '"' : null;
    $person_title_color = ('' != $person_title_color) ? 'style="color: ' . $person_title_color . '"' : null;
    $person_desc_color = ('' != $person_desc_color) ? 'style="color: ' . $person_desc_color . '"' : null;

    $person_title = ('' != $person_title) ? '<small ' . $person_title_color . '>' . $person_title . '</small>' : null;

    $wrapper_class  = implode(' ', $wrapper_class);
    $wrapper_class .= vc_shortcode_custom_css_class($css, ' ');


    ob_start();

    // Start Output
    //////////////////////////////////////////////////////////////////////////////////////////
    ?>

    <?php echo $output_style; ?>

    <div id="<?php echo esc_attr($shortcode_id); ?>" class="person-profile <?php echo esc_attr($wrapper_class); ?>" <?php echo $data_effect ?>>
        <div class="person-profile-image">
            <img src="<?php echo esc_url($person_image[0]); ?>" alt="<?php echo esc_attr($person_name); ?>" />
        </div>

        <div class="person-profile-content">
            <h3 class="person-profile-name" <?php echo $person_name_color; ?>><?php echo $person_name; ?> <?php echo $person_title; ?></h3>
            <p class="person-profile-desc" <?php echo $person_desc_color; ?>><?php echo esc_html($person_desc); ?></p>
            <ul class="person-profile-social-links">
            <?php echo $social_icons; ?>
            </ul>
        </div>

        <div class="clearfix"></div>
    </div>


    <?php
    // End Output
    //////////////////////////////////////////////////////////////////////////////////////////

    $output = ob_get_contents();
    ob_end_clean();


    return $output;

}


/*
    Visual Composer Registration
*/

add_action('vc_before_init', 'leadinjection_person_profile_vc');

function leadinjection_person_profile_vc()
{
    $leadinjection_person_profile_params = array(
        array(
            'type' => 'attach_image',
            'heading' => __('Select Person Image', 'leadinjection'),
            'param_name' => 'person_image',
            'value' => '',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Enter Person Name here', 'leadinjection'),
            'param_name' => 'person_name',
            'admin_label' => true,
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a Person Name Color', 'leadinjection'),
            'param_name' => 'person_name_color',
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Enter Person Title here', 'leadinjection'),
            'param_name' => 'person_title',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a Person Title Color', 'leadinjection'),
            'param_name' => 'person_title_color',
        ),
        array(
            'type' => 'textarea',
            'heading' => __('Enter a Person Short-Description here', 'leadinjection'),
            'param_name' => 'person_desc',
        ),
        array(
            'type' => 'colorpicker',
            'heading' => __('Select a Person Short-Description Color', 'leadinjection'),
            'param_name' => 'person_desc_color',
        ),
        leadinjection_animation_field(),
        leadinjection_css_editor_field(),
        leadinjection_xclass_field(),
        leadinjection_shortcode_id_field(),

        array(
            'type' => 'colorpicker',
            'heading' => __('Social Media Links Color', 'leadinjection'),
            'param_name' => 'social_links_color',
            'description' => __('Enter heading text here', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Facebook URL', 'leadinjection'),
            'param_name' => 'facebook_url',
            'description' => __('Address to your Facebook Profile (e.g. https://www.facebook.com/themeinjection)', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Twitter URL', 'leadinjection'),
            'param_name' => 'twitter_url',
            'description' => __('Address to your Twitter Profile (e.g. https://twitter.com/themeinjection)', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Google Plus URL', 'leadinjection'),
            'param_name' => 'google_plus_url',
            'description' => __('Address to your Google Plus Profile (e.g. https://plus.google.com/+Themeinjection-Themes)', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Public Email Address', 'leadinjection'),
            'param_name' => 'email_address',
            'description' => __('Enter your Email Address here (e.g. mail@themeinjection.com)', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),array(
            'type' => 'textfield',
            'heading' => __('LinkedIn Profile URL', 'leadinjection'),
            'param_name' => 'linkedin_url',
            'description' => __('Address to your LinkedIn Profile (e.g. https://www.linkedin.com/in/themeinjection-a7aa5b1b)', 'leadinjection'),
            'group' => __('Social Media Links', 'leadinjection'),
        ),
    );

    $leadinjection_person_profile_params = leadinjection_add_responsive_helper_params($leadinjection_person_profile_params);

    vc_map(array(
            "name" => __("Person Profile", "leadinjection"),
            "base" => "leadinjection_person_profile",
            "icon" => 'li-icon li-person-profile',
            "class" => '',
            "category" => __("leadinjection", "leadinjection"),
            'description' => __('Person Profile with social links', 'leadinjection'),
            "params" => $leadinjection_person_profile_params
        )
    );
}


