<?php
/**
 * Plugin Name:     Leadinjection ShortCodes
 * Plugin URI:      http://leadinjection.io
 * Description:     Adds Leadinjections ShortCodes to the theme
 * Author:          Themeinjection
 * Author URI:      http://themeinjection.com
 * Version:         1.1.8
 * Text Domain:     leadinjection
 * License:         GPL3+
 * License URI:     http://www.gnu.org/licenses/gpl-3.0.txt
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    die;
}

/**
 * Check if Visual Composer is activated
 */
function leadinjection_vc_check()
{
    if ( !defined('WPB_VC_VERSION') ) {
        $error_msg = '<div class="error" id="messages"><p>';
        $error_msg .= __('The Visual Composer plugin must be installed for the <b>Leadinjection Shortcodes Extension</b> to work.</b>', 'leadinjection');
        $error_msg .= '</p></div>';
        echo $error_msg;
    }
}
add_action('admin_notices', 'leadinjection_vc_check');


/**
 * Create default xclass field
 */
function leadinjection_xclass_field()
{
    $xclass_field = array(
        'type' => 'textfield',
        'heading' => __('Extra class name', 'leadinjection'),
        'description' => __('Style particular content element differently - add a class name and refer to it in custom CSS.', 'leadinjection'),
        'param_name' => 'xclass',
    );

    return $xclass_field;
}

/**
 * Create default icon library field
 */
function leadinjection_icon_library_field()
{
    $icon_lib = array(
        'type' => 'dropdown',
        'heading' => __('Select a Icon library.', 'leadinjection'),
        'value' => array(
            __('Font Awesome', 'leadinjection') => 'fontawesome',
            __('Icons Solid', 'leadinjection') => 'iconssolid',
            __('Open Iconic', 'leadinjection') => 'openiconic',
            __('Typicons', 'leadinjection') => 'typicons',
            __('Entypo', 'leadinjection') => 'entypo',
            __('Linecons', 'leadinjection') => 'linecons',
            __('Custom Image', 'leadinjection') => 'image',
        ),
        'param_name' => 'icon_type',
    );

    return $icon_lib;
}


/**
 * Create default icon fontawesome field
 */
function leadinjection_icon_fontawsome_field()
{
    $fontawesome = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_fontawesome',
        'value' => 'fa fa-adjust',
        'settings' => array(
            'emptyIcon' => false,
            'iconsPerPage' => 4000
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'fontawesome',
        ),
    );

    return $fontawesome;
}

/**
 * Create default icon icons solid field
 */
function leadinjection_icon_iconssolid_field()
{
    $iconssolid = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_iconssolid',
        'value' => 'is is-icon-zynga',
        'settings' => array(
            'emptyIcon' => false,
            'iconsPerPage' => 4000,
            'type' => 'iconssolid',
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'iconssolid',
        ),
    );

    return $iconssolid;
}

/**
 * Create default icon openiconic field
 */
function leadinjection_icon_openiconic_field()
{
    $openiconic = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_openiconic',
        'value' => 'vc-oi vc-oi-dial',
        'settings' => array(
            'emptyIcon' => false,
            'type' => 'openiconic',
            'iconsPerPage' => 4000,
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'openiconic',
        ),
    );

    return $openiconic;
}


/**
 * Create default icon typicons field
 */
function leadinjection_icon_typicons_field()
{
    $typicons = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_typicons',
        'value' => 'typcn typcn-adjust-brightness',
        'settings' => array(
            'emptyIcon' => false,
            'type' => 'typicons',
            'iconsPerPage' => 4000,
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'typicons',
        ),
    );

    return $typicons;
}


/**
 * Create default icon entypo field
 */
function leadinjection_icon_entypo_field()
{
    $entypo = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_entypo',
        'value' => 'entypo-icon entypo-icon-note',
        'settings' => array(
            'emptyIcon' => false,
            'type' => 'entypo',
            'iconsPerPage' => 4000,
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'entypo',
        ),
    );

    return $entypo;
}


/**
 * Create default icon linecons field
 */
function leadinjection_icon_linecons_field()
{
    $linecons = array(
        'type' => 'iconpicker',
        'heading' => __('Select a Icon.', 'leadinjection'),
        'param_name' => 'icon_linecons',
        'value' => 'vc_li vc_li-heart',
        'settings' => array(
            'emptyIcon' => false,
            'type' => 'linecons',
            'iconsPerPage' => 4000,
        ),
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'linecons',
        ),
    );

    return $linecons;
}


/**
 * Create default icon image field
 */
function leadinjection_icon_image_field()
{
    $image = array(
        'type' => 'attach_image',
        'heading' => __('Select custom icon image.', 'leadinjection'),
        'param_name' => 'icon_image',
        'value' => '',
        'dependency' => array(
            'element' => 'icon_type',
            'value' => 'image',
        ),

    );

    return $image;
}


/**
 * Create default animation field
 */
function leadinjection_animation_field()
{
    $animation_field = array(
        'type' => 'animation_style',
        'heading' => __('Select a animation style', 'leadinjection'),
        'param_name' => 'animation',
    );

    return $animation_field;
}

/**
 * Create default css editor field
 */
function leadinjection_css_editor_field()
{
    $css_editor_field = array(
        'type' => 'css_editor',
        'heading' => __('Css', 'leadinjection'),
        'param_name' => 'css',
        'group' => __('Design options', 'leadinjection'),
    );

    return $css_editor_field;
}

/**
 * Create default element ID field
 */
function leadinjection_shortcode_id_field()
{
    $xclass_field = array(
        'type' => 'textfield',
        'heading' => __('Shortcode ID', 'leadinjection'),
        'description' => __('Enter shortcode ID (Note: make sure it is unique and valid according to w3c specification).', 'leadinjection'),
        'param_name' => 'shortcode_id',
        'value' => '',
    );

    return $xclass_field;
}

/**
 * Create a custom element ID
 */
function leadinjection_custom_id($prefix = 'custom-', $shortcode_id)
{
    $shortcode_id = ('' === $shortcode_id) ? $prefix . uniqid() : $shortcode_id;
    return $shortcode_id;
}

/**
 * Enqueue styles and scripts for aninmtion
 */
function leadinjection_enqueue_animation()
{
    wp_enqueue_script('waypoints');
}

/**
 * Enqueue styles and scripts for counter
 */
function leadinjection_enqueue_counter()
{
    wp_enqueue_script('waypoints');
    wp_enqueue_script('animate-number');
}

/**
 * Get attributes of nested shortcodes
 */
function leadinjection_attribute_map($str, $att = null)
{
    $res = array();
    $return = array();
    $reg = get_shortcode_regex();
    preg_match_all('~' . $reg . '~', $str, $matches);
    foreach ($matches[2] as $key => $name) {
        $parsed = shortcode_parse_atts($matches[3][$key]);
        $parsed = is_array($parsed) ? $parsed : array();
        $res[$name] = $parsed;
        $return[] = $res;
    }
    return $return;
}

/**
 * Get all Modals
 */
function leadinjection_get_modals()
{
    $query = new WP_Query(array( 'post_type' => 'li_modals', 'posts_per_page'=> -1 ));

    $modals = array();
    foreach ($query->posts as $modal) {
        $modals['[ #liModal-' . $modal->ID . ' ] - ' . $modal->post_title] = $modal->ID;
    }

    return $modals;
}

/**
 * Get all CF7 Forms
 */
function leadinjection_get_cf7_forms()
{
    $query = new WP_Query(array('post_type' => 'wpcf7_contact_form'));

    $forms = array();
    foreach ($query->posts as $form) {
        $forms[$form->post_title] = $form->post_name;
    }

    return $forms;
}


/**
 * Load Responsive Helper Options
 */
require_once(plugin_dir_path(__FILE__) . '/leadinjection_shortcodes_responsive_helper.php');



/**
 *  Extend Default VC Elements Params
 */
function leadinjection_extend_default_vc_params()
{
    // Row Alignment
    $param = array(
        'type' => 'dropdown',
        'heading' => __('Select content alignment', 'leadinjection'),
        'admin_label' => true,
        'param_name' => 'content_align',
        'value' => array(
            __('None', 'leadinjection') => '',
            __('Left', 'leadinjection') => 'text-left',
            __('Center', 'leadinjection') => 'text-center',
            __('Right', 'leadinjection') => 'text-right',
        ));

    vc_add_param('vc_row', $param);

    // Row Hidden Overflow
    $param = array(
        'type' => 'checkbox',
        'param_name' => 'overflow',
        'value' => array(__('Display overflow hidden', 'leadinjection') => 'of-hidden')
    );

    vc_add_param('vc_row', $param);


    // Row Animation
    vc_add_param('vc_row', leadinjection_animation_field());

    // Column Animation
    vc_add_param('vc_column', leadinjection_animation_field());

    //
    vc_add_param("vc_row", array(
        "type" => "dropdown",
        "heading" => "Background position",
        "param_name" => "background_position",
        "value" => array(
            'None' => 'None',
            "center center" => "center center",
            "center top" => "center top",
            "center bottom" => "center bottom"
        )
    ));

}



/**
 *  Load Leadinjection Params Shortcodes
 */
if (defined('WPB_VC_VERSION')) {

    add_action('vc_after_init', 'leadinjection_extend_default_vc_params');

    require_once(plugin_dir_path(__FILE__) . '/shortcodes/side_icon_text.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/heading.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/feature_icon_text.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/image_testimonial_slider.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/person-profile.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/icon_list.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/number_counter.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/textblock.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/button.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/pricing_table_simple.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/icon.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/navbar.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/accordion.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/tabs.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/image.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/video.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/rating_slider.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/location_map.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/icon_text_box.php');
    require_once(plugin_dir_path(__FILE__) . '/shortcodes/countdown.php');

    // Init Icons Solid for Iconpicker
    require_once(plugin_dir_path(__FILE__) . '/iconssolid_init.php');

    if (function_exists('create_post_type_li_modals')) {
        require_once(plugin_dir_path(__FILE__) . '/shortcodes/modal.php');
    }
}


