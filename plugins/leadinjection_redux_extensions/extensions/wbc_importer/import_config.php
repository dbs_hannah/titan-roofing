<?php

/************************************************************************
 * Extended Example:
 * Way to set menu, import revolution slider, and set home page.
 *************************************************************************/
if ( !function_exists( 'wbc_extended_example' ) ) {
    function wbc_extended_example( $demo_active_import , $demo_directory_path ) {
        reset( $demo_active_import );
        $current_key = key( $demo_active_import );
        /************************************************************************
         * Import slider(s) for the current demo being imported
         *************************************************************************/
        if ( class_exists( 'RevSlider' ) ) {
            //If it's demo3 or demo5
            $wbc_sliders_array = array(
                'Application' => 'application-slider.zip', //Set slider zip name
                'Online-Course' => 'online-course-slider.zip', 
                'Medical-Services' => 'medical-slider.zip',
                'Ebook' => 'ebook-slider.zip',
                'SEO-Service' => 'seo-service-slider.zip',
            );
            if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_sliders_array ) ) {
                $wbc_slider_import = $wbc_sliders_array[$demo_active_import[$current_key]['directory']];
                if ( file_exists( $demo_directory_path.$wbc_slider_import ) ) {
                    $slider = new RevSlider();
                    $slider->importSliderFromPost( true, true, $demo_directory_path.$wbc_slider_import );
                }
            }
        }
        /************************************************************************
         * Setting Menus
         *************************************************************************/
        // If it's demo1 - demo6
        $wbc_menu_array = array(
            'Application' => 'Main Menu Top',
            'Conference' => 'Conference Main Menu Top',
            'Medical-Services' => 'Medical Navigation Top',
            'Ebook' => 'Ebook Top Navigation',
            'SEO-Service' => 'SEO Top Navigation'
        );

        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_menu_array ) ) {
            $top_menu = get_term_by( 'name', $wbc_menu_array[$demo_active_import[$current_key]['directory']], 'nav_menu' );

            $footer_widget_menu = get_term_by( 'name', 'Footer Widget Menu', 'nav_menu' );
            if ( isset( $top_menu->term_id ) ) {
                // Set Primary Nav
                set_theme_mod( 'nav_menu_locations', array(
                        'primary' => $top_menu->term_id
                    )
                );
            }
        }
        /************************************************************************
         * Set HomePage
         *************************************************************************/
        // array of demos/homepages to check/select from
        $wbc_home_pages = array(
            'Application' => 'Application Landing Page',
            'Online-Course' => 'Online Courses Landing Page',
            'Conference' => 'Conference Landing Page',
            'Medical-Services' => 'Medical Landing Page',
            'Ebook' => 'Ebook Landing Page',
            'SEO-Service' => 'SEO Service Landing Page',
        );
        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
            $page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
            if ( isset( $page->ID ) ) {
                update_option( 'page_on_front', $page->ID );
                update_option( 'show_on_front', 'page' );
            }
        }
        /************************************************************************
         * Set BlogPage
         *************************************************************************/
        // array of demos/homepages to check/select from
        $wbc_home_pages = array(
            'Application' => 'Blog',
            'demo2' => 'Home Page v2',
            'demo3' => 'Home Page v3',
            'demo4' => 'Home Page v4',
            'demo5' => 'Home Page v5',
            'demo6' => 'Home Page v6',
        );
        if ( isset( $demo_active_import[$current_key]['directory'] ) && !empty( $demo_active_import[$current_key]['directory'] ) && array_key_exists( $demo_active_import[$current_key]['directory'], $wbc_home_pages ) ) {
            $page = get_page_by_title( $wbc_home_pages[$demo_active_import[$current_key]['directory']] );
            if ( isset( $page->ID ) ) {
                update_option( 'page_for_posts', $page->ID );
            }
        }
    }

    add_action( 'wbc_importer_after_content_import', 'wbc_extended_example', 10, 2 );


    /**
     * Clean Wordpress for Demo Content
     */
    function leadinjection_clean_wp_before_import(){

        global $wpdb;

        $removed = array();
        if($wpdb->query("TRUNCATE TABLE $wpdb->posts")) $removed[] = 'Posts removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->postmeta")) $removed[] = 'Postmeta removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->comments")) $removed[] = 'Comments removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->commentmeta")) $removed[] = 'Commentmeta removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->links")) $removed[] = 'Links removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->terms")) $removed[] = 'Terms removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->term_relationships")) $removed[] = 'Term relationships removed';
        if($wpdb->query("TRUNCATE TABLE $wpdb->term_taxonomy")) $removed[] = 'Term Taxonomy removed';
        if($wpdb->query("DELETE FROM $wpdb->options WHERE `option_name` LIKE ('_transient_%')")) $removed[] = 'Transients removed';
        if($wpdb->query("DELETE FROM $wpdb->options WHERE `option_name` LIKE ('wbc_imported_demos')")) $removed[] = 'WBC Imported Demos Removed';
        if($wpdb->query("DELETE FROM $wpdb->options WHERE `option_name` LIKE ('sidebars_widgets')")) $removed[] = 'Sidebar Widgets Removed';
        $wpdb->query("OPTIMIZE TABLE $wpdb->options");

        //var_dump($removed);
    }

    add_action( 'import_start', 'leadinjection_clean_wp_before_import' );



    function lp_wbc_importer_description(){

        $importer_desc      = '<p class="importer-notice"><strong>Important:</strong> Each landing page ships with individual configuration and options. Importing data is recommended on fresh installs only. If you import or re-import a demo it will be overwrite your current posts, pages, configurations, options, images etc. All Images are for demo purpose only. ';
        $importer_desc .= 'Before importing demo content, please <a href="https://codex.wordpress.org/WordPress_Backups">back up your database and files</a>.</p>';
        
        return $importer_desc;
    }

    add_filter( 'wbc_importer_description', 'lp_wbc_importer_description', 10, 3 );


}
