<?php

return array(

    /**
     * Modal Options
     */
    array(
        'title' => __('Modal Options', 'leadinjection'),
        'icon' => 'el el-cog', // Only used with metabox position normal or advanced
        'fields' => array(
            array(
                'id'       => 'li_modal_dimensions',
                'type'     => 'dimensions',
                'units'    => array('px','%'),
                'title'    => __('Dimensions (Width/Height) Option', 'redux-framework-demo'),
                'subtitle' => __('Allow your users to choose width, height, and/or unit.', 'redux-framework-demo'),
                'desc'     => __('Enable or disable any piece of this field. Width, Height, or Units.', 'redux-framework-demo'),
                'default'  => array(
                    'Width'   => '600',
                    'Height'  => '500'
                )
            ),
        ),
    ),


    /**
     * Modal Options
     */
    //    array(
    //        'title' => __('Footer Options', 'leadinjection'),
    //        'id' => 'modal-footer-options',
    //        'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    //        'icon' => 'el el-adjust-alt',
    //        'fields' => array(
    //            array(
    //                'id' => 'li-modal-footer-widget-bar-color',
    //                'type' => 'background',
    //                'title' => __('Widget Bar Background Color/Image', 'leadinjection'),
    //                'subtitle' => __('Select the background color or an image for the footer widget bar..', 'leadinjection'),
    //                'output' => array('.site-footer .footer-widget-bar'),
    //            ),
    //        )
    //    ),

);