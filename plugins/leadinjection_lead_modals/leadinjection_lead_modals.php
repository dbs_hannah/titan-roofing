<?php
/**
 * Plugin Name:     Leadinjection - Lead Modals
 * Plugin URI:      http://leadinjection.io
 * Description:     Adds custom post type leadModals to the theme.
 * Author:          Themeinjection
 * Author URI:      http://themeinjection.com
 * Version:         1.1.8
 * Text Domain:     leadinjection
 * License:         GPL3+
 * License URI:     http://www.gnu.org/licenses/gpl-3.0.txt
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    die;
}

/**
 * Register Modal Post Type
 */
add_action('init', 'create_post_type_li_modals');
function create_post_type_li_modals()
{
    register_post_type('li_modals',
        array(
            'labels' => array(
                'name' => __('Lead Modals', 'leadinjection'),
                'singular_name' => __('Lead Modal', 'leadinjection')
            ),
            'public' => true,
            'has_archive' => true,
            'menu_icon' => 'dashicons-share-alt2',
        )
    );
}


