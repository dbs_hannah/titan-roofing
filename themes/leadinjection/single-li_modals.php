<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package leadinjection
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" >



        <div class="container">
            <div class="row">
                <!--start main content-->
                <div class="col-md-12">
                    <?php while (have_posts()) : the_post(); ?>

                        <?php if('li_modals' == get_post_type()) : ?>
                        <script type="text/javascript">
                            jQuery(window).load(function(){
                                jQuery('#previewModal').modal('show');
                            });
                        </script>
                        <?php endif; ?>

                        <div class="modal fade li-modal" id="previewModal" tabindex="-1" role="dialog">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <div class="modal-body">
                                        <?php echo the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br><br><br><br><br><br><br><br><br>
                        <!-- Button trigger modal -->
                        <div class="row">
                            <div class="col-md-12 text-center" >
                                <button type="button" class="btn btn-green btn-lg" data-toggle="modal" data-target="#previewModal">
                                    Launch demo modal
                                </button>
                            </div>
                        </div>

                        <br><br><br><br><br><br><br><br><br>
                        <br><br><br><br><br><br><br><br><br>

                    <?php endwhile; // End of the loop. ?>
                </div>
                <!--end main content-->


            </div>
        </div>


    </main>

</div>

<?php get_footer(); ?>
