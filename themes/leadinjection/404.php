<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package leadinjection
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="page-title">
				<div class="container">
					<div class="row">
						<div class="col-md-6"><h1><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'leadinjection' ); ?></h1></div>
						<div class="col-md-6">
							<?php custom_breadcrumbs(); ?>
						</div>
					</div>
				</div>
			</section>

			<section class="error-404 not-found">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2><?php esc_html_e( 'Oops, This Page Could Not Be Found!', 'leadinjection' ); ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-5">
							<div class="error-number">404</div>
						</div>
						<div class="col-md-5 col-md-offset-1 error-search">
							<h3>
								<?php esc_html_e( "Can't find what you need?", 'leadinjection' ); ?><br/>
								<span><?php esc_html_e( "Take a moment and do a search below!", 'leadinjection' ); ?></span>
							</h3>
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
