<?php
/**
 * Template part for displaying single posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package leadinjection
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<?php if (has_post_thumbnail()) : ?>
			<div class="featured-image">
				<a href="<?php echo get_permalink(); ?>" >
					<?php the_post_thumbnail('leadinjection-featured-image'); ?>
				</a>
			</div>
		<?php endif; ?>

		<?php the_title( '<h2 class="entry-title">', '</h2>' ); ?>

		<div class="entry-meta">
			<?php leadinjection_posted_on_sp(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>



		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'leadinjection' ),
				'after'  => '</div>',
			) );
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php leadinjection_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->

