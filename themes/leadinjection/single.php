<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package leadinjection
 */

// leadinjection Global options
$leadinjection_global_option = get_option( 'rdx_option' );

get_header();
?>
<div id="primary" class="content-area">
    <main id="main" class="site-main">

        <section class="page-title">
            <div class="container">
                <div class="row">
                    <div class="col-md-6"><h1>
                            <?php if (empty($leadinjection_global_option["li-global-blog-title"])) {
                                echo __('Blog', 'leadinjection');
                            } else {
                                echo $leadinjection_global_option["li-global-blog-title"];
                            } ?>
                        </h1></div>
                    <div class="col-md-6">
                        <?php custom_breadcrumbs(); ?>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="row">
                <!--start main content-->
                <div class="col-md-8">
                    <?php while (have_posts()) : the_post(); ?>

                        <?php get_template_part('template-parts/content', 'single'); ?>

                        <?php if (get_the_author_meta('description') !== '') : ?>
                            <div class="author-post">
                                <div class="author-img">
                                    <?php echo get_avatar($post->post_author, 120); ?>
                                </div>
                                <div class="author-content">
                                    <h2><?php echo __('About the Author: ', 'leadinjection') . get_the_author_meta('display_name'); ?></h2>

                                    <p><?php echo get_the_author_meta('description'); ?></p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        <?php endif; ?>

                        <?php
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;
                        ?>

                    <?php endwhile; // End of the loop. ?>
                </div>
                <!--end main content-->

                <!--start sidebar-->
                <div class="col-md-4">
                    <?php get_sidebar(); ?>
                </div>
                <!--end sidebar-->
            </div>
        </div>


    </main>

</div>

<?php get_footer(); ?>
