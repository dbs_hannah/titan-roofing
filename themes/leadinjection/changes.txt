05.01.2017 - ver 1.1.8
   - Update: WPBakery Visual Composer plugin to Version 5.0.1
   - Update: Redux Framework plugin to Version 3.6.3
   - Update: Contact Form 7 plugin to Version 4.6
   - Update: Slider Revolution plugin to Version 5.3.1.5
   - Fixed: Skype URl validation
   - Fixed: Navigation second level

28.09.2016 - ver 1.1.7
   - Update: WPBakery Visual Composer plugin to Version 4.12.1
   - Update: Redux Framework plugin to Version 3.6.2
   - Update: Contact Form 7 plugin to Version 4.5.1

12.08.2016 - ver 1.1.6
   - New: Added new landing page "SEO Service"
   - Modified: Icon Shortcode
   - Add: Custom Button Styles
   - Add: Added Page Topbar
   - Add: Added Icons Solid Font (save $39)
   - Fixed: Location map API issue
   - Update: Contact Form 7 plugin to Version 4.5

05.07.2016 - ver 1.1.5
   - New: Added new landing page "eBook"
   - Update: Slider Revolution plugin to Version 5.2.6
   - Add: Responsive Helpers to short Codes
   - Add: Back to Top Button
   - Modified: plugin loading
   - Modified: improve demo importer
   - Fixed: blog page on-page options
   - Fixed: minor bug fixes

14.06.2016 - ver 1.1.4
   - Update: WPBakery Visual Composer plugin to Version 4.12
   - Update: Slider Revolution plugin to Version 5.2.5.3

09.05.2016 - ver 1.1.3
   - Update: Slider Revolution plugin to Version 5.2.5.1
   - Update: Contact Form 7 plugin to Version 4.4.2
   - Add: Linkedin icon to "person-profile" shortcode
   - Fixed: minor bug fixes

27.04.2016 - ver 1.1.2
   - New: Added new landing page "Medical Services"
   - Add: Animated “x” icon for the navbar-toggle
   - Add: Font Weight settings to headings shortcode
   - Add: Text Color settings to feature icon shortcode
   - Modified: removed auto <script> tag from raw header and footer output
   - Update: Redux Framework plugin to Version 3.6.0.2
   - Fixed: minor bug fixes

18.04.2016 - ver 1.1.1
   - Update: WPBakery Visual Composer plugin to Version 4.11.2.1
   - Update: Slider Revolution plugin to Version 5.2.5
   - Update: Contact Form 7 plugin to Version 4.4.1
   - Update: Redux Framework plugin to Version 3.6.0

   - Fixed: minor bug fixes

15.03.2016 - ver 1.1.0
   - Update: WPBakery Visual Composer plugin to Version 4.11
   - Update: Slider Revolution plugin to Version 5.2.2
   - Added: block style to buttons shortcode
   - Modified: video shortcode
   - Modified: default plugin loading
   - Modified: minor bug fixes

03.03.2016 - ver 1.0.0
   - initial release