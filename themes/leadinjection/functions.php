<?php
/**
 * leadinjection functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package leadinjection
 *
 *
 */


if (!function_exists('leadinjection_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function leadinjection_setup()
    {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on leadinjection, use a find and replace
         * to change 'leadinjection' to the name of your theme in all the template files.
         */
        load_theme_textdomain('leadinjection', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary Menu', 'leadinjection'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        /*
         * Enable support for Post Formats.
         * See https://developer.wordpress.org/themes/functionality/post-formats/
         */
        add_theme_support('post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('leadinjection_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));
    }
endif; // leadinjection_setup
add_action('after_setup_theme', 'leadinjection_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function leadinjection_content_width()
{
    $GLOBALS['content_width'] = apply_filters('leadinjection_content_width', 640);
}

add_action('after_setup_theme', 'leadinjection_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function leadinjection_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'leadinjection'),
        'id' => 'sidebar-1',
        'description' => '',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'leadinjection_widgets_init');


///**
// * Add search-filter
// */
//function SearchFilter($query)
//{
//    if ($query->is_search) {
//        $query->set('post_type', 'post');
//        $query->set('post_type', 'li_leads');
//    }
//
//    if ( in_array ( $query->get('post_type'), array('li_leads') ) ) {
//        $query->set( 'meta_key', '_ct_selectbox_52f65ae267764' );
//        $query->set( 'meta_value', $city );
//        return;
//    }
//    return $query;
//}
//
//add_filter('pre_get_posts', 'SearchFilter');

/**
 * Register scripts and styles.
 */
function leadinjection_register_scripts()
{
    // Register scripts
    //wp_register_script('li_waypoints', get_template_directory_uri() . '/bower_components/waypoints/lib/jquery.waypoints.min.js', array('jquery'), false, true);
    wp_register_script('animate-number', get_template_directory_uri() . '/bower_components/jquery-animateNumber/jquery.animateNumber.min.js');
    wp_register_script('countdown', get_template_directory_uri() . '/bower_components/jquery.countdown/dist/jquery.countdown.min.js');
    wp_register_style('imagehover', get_template_directory_uri() . '/bower_components/imagehover.css/css/imagehover.min.css');
}

add_action('template_redirect', 'leadinjection_register_scripts', 5);


/**
 * Enqueue scripts and styles.
 */
function leadinjection_enqueue_scripts()
{
    // Enqueue CSS
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/bower_components/bootstrap/dist/css/bootstrap.min.css');
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/bower_components/fontawesome/css/font-awesome.min.css');
    wp_enqueue_style('animate', get_template_directory_uri() . '/bower_components/animate.css/animate.min.css');
    wp_enqueue_style('leadinjection-style', get_stylesheet_uri());

    // Enqueue JS
    wp_enqueue_script('bootstrap', get_template_directory_uri() . '/bower_components/bootstrap/dist/js/bootstrap.min.js', array('jquery'), false, true);

    // Load Scrollspy Fix for FrontPage
    if(is_front_page()){
        wp_enqueue_script('leadinjection-scrollspy', get_template_directory_uri() . '/js/scrollspy.js', array('jquery'), false, true);
    }

    wp_enqueue_script('leadinjection-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), false, true);
    wp_enqueue_script('skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true);

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

add_action('wp_enqueue_scripts', 'leadinjection_enqueue_scripts');


/**
 * Enqueue scripts and styles backend.
 */
function leadinjection_enqueue_scripts_backend()
{
    wp_enqueue_style('fontawesome', get_template_directory_uri() . '/bower_components/fontawesome/css/font-awesome.min.css');
    wp_enqueue_style('leadinjection-vc-icons-css', get_template_directory_uri() . '/backend/css/backend-styles.css');
    wp_enqueue_style('leadinjection-backend-css', get_template_directory_uri() . '/backend/css/li-vc-icon-sprite.css');
}

add_action('admin_enqueue_scripts', 'leadinjection_enqueue_scripts_backend');


/**
 * Add IE8 support
 */
function leadinjection_add_ie8_support()
{
    $output  = '<script src="' . get_template_directory_uri() . '/bower_components/html5shiv/dist/html5shiv.min.js"></script>';
    $output .= '<script src="' . get_template_directory_uri() .  '/bower_components/respond/dest/respond.min.js"></script>';
    echo $output;
}

add_action('leadinjection_ie8_support', 'leadinjection_add_ie8_support');


/**
 * Add custom editor stlyes
 */
function leadinjection_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}

add_action( 'admin_init', 'leadinjection_editor_styles' );


/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Custom Breadcrumbs
 */
require get_template_directory() . '/inc/custom-breadcrumbs.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';


// Register Custom Navigation Walker
require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');

// Add Featured Image Size (750px x 350px )
add_image_size('leadinjection-featured-image', 750, 350, true);

// Add Person Profile Image Size (261px x 261px )
add_image_size('leadinjection-person-profile', 261, 261, true);

// Register Footerbar
function leadinjection_add_footer_columns(){

    register_sidebar(array('name' => 'Footer 1 Column',
        'id' => 'footer-widget-bar-col1',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));

    register_sidebar(array('name' => 'Footer 2 Column',
        'id' => 'footer-widget-bar-col2',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));

    register_sidebar(array('name' => 'Footer 3 Column',
        'id' => 'footer-widget-bar-col3',
        'before_widget' => '<div class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    ));
}
add_action( 'widgets_init', 'leadinjection_add_footer_columns' );


// Style Comment Fields
add_filter('comment_form_default_fields', 'leadinjection_bootstrap3_comment_form_fields');
function leadinjection_bootstrap3_comment_form_fields($fields)
{
    $commenter = wp_get_current_commenter();

    $req = get_option('require_name_email');
    $aria_req = ($req ? " aria-required='true'" : '');
    $html5 = current_theme_supports('html5', 'comment-form') ? 1 : 0;

    $fields = array(
        'author' => '<div class="form-group comment-form-author">' . '<label for="author">' . __('Name', 'leadinjection') . ($req ? ' <span class="required">*</span>' : '') . '</label> ' .
            '<input class="form-control input-md invert" id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" placeholder="' . __('Name (required):', 'leadinjection') . '" size="30"' . $aria_req . ' /></div>',
        'email' => '<div class="form-group comment-form-email"><label for="email">' . __('Email', 'leadinjection') . ($req ? ' <span class="required">*</span>' : '') . '</label> ' .
            '<input class="form-control input-md invert" id="email" name="email" ' . ($html5 ? 'type="email"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_email']) . '" placeholder="' . __('Email (required):', 'leadinjection') . '" size="30"' . $aria_req . ' /></div>',
        'url' => '<div class="form-group comment-form-url"><label for="url">' . __('Website', 'leadinjection') . '</label> ' .
            '<input class="form-control input-md invert" id="url" name="url" ' . ($html5 ? 'type="url"' : 'type="text"') . ' value="' . esc_attr($commenter['comment_author_url']) . '" placeholder="' . __('Website', 'leadinjection') . '" size="30" /></div>'
    );

    return $fields;
}

// Style Comment Textarea and Button
add_filter('comment_form_defaults', 'leadinjection_bootstrap3_comment_form');
function leadinjection_bootstrap3_comment_form($args)
{
    $args['comment_field'] = '<div class="form-group comment-form-comment">
            <label for="comment">' . _x('Comment', 'noun', 'leadinjection') . '</label>
            <textarea class="form-control input-md invert" id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="' . __('Message (required):', 'leadinjection') . '"></textarea>
        </div>';
    $args['class_submit'] = 'btn btn-md btn-red';
    return $args;
}


/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'leadinjection_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function leadinjection_register_required_plugins() {
    /*
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        array(
            'name' => 'Redux Framework',
            'slug' => 'redux-framework',
            'source' => get_stylesheet_directory() . '/inc/plugins/redux-framework.3.6.3.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Leadinjection Redux Extensions',
            'slug' => 'leadinjection_redux_extensions',
            'source' => get_stylesheet_directory() . '/inc/plugins/leadinjection_redux_extensions.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Visual Composer',
            'slug' => 'js_composer',
            'source' => get_stylesheet_directory() . '/inc/plugins/js_composer.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Leadinjection ShortCodes',
            'slug' => 'leadinjection_shortcodes',
            'source' => get_stylesheet_directory() . '/inc/plugins/leadinjection_shortcodes.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Leadinjection - Lead Modals',
            'slug' => 'leadinjection_lead_modals',
            'source' => get_stylesheet_directory() . '/inc/plugins/leadinjection_lead_modals.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Slider Revolution',
            'slug' => 'slider-revolution',
            'source' => get_stylesheet_directory() . '/inc/plugins/revslider.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'source' => get_stylesheet_directory() . '/inc/plugins/contact-form-7.4.6.zip',
            'required' => true,
            'version' => '',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        )
    
    );

    /*
     * Array of configuration settings. Amend each line as needed.
     *
     * TGMPA will start providing localized text strings soon. If you already have translations of our standard
     * strings available, please help us make TGMPA even better by giving us access to these translations or by
     * sending in a pull-request with .po file(s) with the translations.
     *
     * Only uncomment the strings in the config array if you want to customize the strings.
     */
    $config = array(
        'id'           => 'leadinjection',             // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => __('Install Required Plugins', 'leadinjection'),
            'menu_title' => __('Install Plugins', 'leadinjection'),
            'installing' => __('Installing Plugin: %s', 'leadinjection'), // %1$s = plugin name
            'oops' => __('Something went wrong with the plugin API.', 'leadinjection'),
            'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'leadinjection'),
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'leadinjection'),
            'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'leadinjection'),
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'leadinjection'),
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'leadinjection'),
            'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'leadinjection'),
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'leadinjection'),
            'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'leadinjection'),
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'leadinjection'),
            'activate_link' => _n_noop('Activate installed plugin', 'Activate installed plugins', 'leadinjection'),
            'return' => __('Return to Required Plugins Installer', 'leadinjection'),
            'plugin_activated' => __('Plugin activated successfully.', 'leadinjection'),
            'complete' => __('All plugins installed and activated successfully. %s', 'leadinjection'),
            'nag_type' => 'updated'
        )
    );

    tgmpa( $plugins, $config );
}


/**
 *  Init Visual Composer
 */
require_once(get_template_directory() . '/inc/visual_composer_init.php');


/**
 *  Hide activation and update of Slider Revolution.
 */
if(function_exists( 'set_revslider_as_theme' )){
    add_action( 'init', 'leadinjection_set_revslider_activation' );
    function leadinjection_set_revslider_activation() {
        set_revslider_as_theme();
    }
}


/**
 * Load Redux Framework Options.
 */
if ( class_exists('ReduxFramework' ) ){
    require get_template_directory() . '/backend/redux-options-init.php';
}

/**
 * Disable Redux Framework Demo Mode.
 */
function leadinjection_remove_redux_demo() {
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2 );
    }
    if ( class_exists('ReduxFrameworkPlugin') ) {
        remove_action('admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
    }
}
add_action('init', 'leadinjection_remove_redux_demo');



/**
 * Disable Contactform 7 styles.css
 */
add_filter( 'wpcf7_load_css', '__return_false' );



/**
 * Disable Dashicons from the frontend
 */
function leadinjection_deregister_dashicons()    {
    if( !is_user_logged_in() )
        wp_deregister_style( 'dashicons');
}
add_action( 'wp_print_styles', 'leadinjection_deregister_dashicons', 100 );


/**
 * Remove Rev Slider Metabox
 */
if ( is_admin() ) {

    function leadinjection_remove_revslider_meta_boxes() {
        remove_meta_box( 'mymetabox_revslider_0', 'page', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'post', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'li_modals', 'normal' );
        remove_meta_box( 'mymetabox_revslider_0', 'li_leads', 'normal' );
    }

    add_action( 'do_meta_boxes', 'leadinjection_remove_revslider_meta_boxes' );

}


/**
 * Disable VisualComposer activation massage
 */
setcookie('vchideactivationmsg', '1', strtotime('+3 years'), '/');
setcookie('vchideactivationmsg_vc11', (defined('WPB_VC_VERSION') ? WPB_VC_VERSION : '1'), strtotime('+3 years'), '/');
