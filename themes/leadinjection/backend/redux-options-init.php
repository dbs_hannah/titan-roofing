<?php

/**
 * For full documentation, please visit: http://docs.reduxframework.com/
 * For a more extensive sample-config file, you may look at:
 * https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
 */

if (!class_exists('Redux')) {
    return;
}

// This is your option name where all the Redux data is stored.
$opt_name = "rdx_option";

/**
 * ---> SET ARGUMENTS
 * All the possible arguments for Redux.
 * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
 * */

$theme = wp_get_theme(); // For use with some settings. Not necessary.

$args = array(
    'opt_name' => $opt_name,
    'display_name' => 'Leadinjection',
    'display_version' => '1.1.8',
    'page_slug' => 'leadinjection-options',
    'page_title' => 'leadinjection Options',
    'update_notice' => TRUE,
    'admin_bar' => TRUE,
    'menu_type' => 'menu',
    'menu_title' => 'Leadinjection',
    'admin_bar_icon' => 'dashicons-admin-generic',
    'menu_icon' => get_template_directory_uri() . '/img/wp-nav-icon.png',
    'allow_sub_menu' => TRUE,
    'page_parent_post_type' => 'your_post_type',
    'customizer' => TRUE,
    'default_mark' => '*',
    'hints' => array(
        'icon_position' => 'right',
        'icon_size' => 'normal',
        'tip_style' => array(
            'color' => 'light',
        ),
        'tip_position' => array(
            'my' => 'top left',
            'at' => 'bottom right',
        ),
        'tip_effect' => array(
            'show' => array(
                'duration' => '500',
                'event' => 'mouseover',
            ),
            'hide' => array(
                'duration' => '500',
                'event' => 'mouseleave unfocus',
            ),
        ),
    ),
    'output' => TRUE,
    'output_tag' => TRUE,
    'settings_api' => TRUE,
    'cdn_check_time' => '1440',
    'compiler' => TRUE,
    'page_permissions' => 'manage_options',
    'save_defaults' => TRUE,
    'show_import_export' => TRUE,
    'database' => 'options',
    'transient_time' => '3600',
    'network_sites' => TRUE,
    'dev_mode' => FALSE,
);

Redux::setArgs($opt_name, $args);

/*
 * ---> END ARGUMENTS
 */

/*
 * ---> START HELP TABS
 */

$tabs = array(
    array(
        'id' => 'redux-help-tab-1',
        'title' => __('Theme Information 1', 'admin_folder'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'admin_folder')
    ),
    array(
        'id' => 'redux-help-tab-2',
        'title' => __('Theme Information 2', 'admin_folder'),
        'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'admin_folder')
    )
);
Redux::setHelpTab($opt_name, $tabs);

// Set the help sidebar
$content = __('<p>This is the sidebar content, HTML is allowed.</p>', 'admin_folder');
Redux::setHelpSidebar($opt_name, $content);


/*
 * <--- END HELP TABS
 */


/*
 *
 * ---> START SECTIONS
 *
 */


Redux::setSection($opt_name, array(
    'title' => __('Default Options', 'leadinjection'),
    'id' => 'global-options',
    'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-home',
    'fields' => array(
        array(
            'id' => 'li-global-typography',
            'type' => 'typography',
            'title' => __('Typography', 'leadinjection'),
            'google' => true,
            'font-backup' => true,
            'output' => array('body'),
            'units' => 'px',
            'all_styles' => true,
            'subtitle' => __('Typography option with each property can be called individually.', 'leadinjection'),
            'default' => array(
                'color' => '#575756',
                'font-style' => '400',
                'font-family' => 'Lato',
                'google' => true,
                'font-size' => '16px',
                'line-height' => '28px'
            ),
        ),
        array(
            'id' => 'li-global-color',
            'type'     => 'color',
            'title'    => __('Default Page color', 'leadinjection'),
            'subtitle' => __('Select a default page color.', 'leadinjection'),
            'validate' => 'color',
            'output' => require get_template_directory() . '/backend/redux-default-color.php',
        ),
        array(
            'id'       => 'li-global-link-color',
            'type'     => 'link_color',
            'title'    => __('Default Link color', 'leadinjection'),
            'subtitle' => __('Select a default link color.', 'leadinjection'),
            'visited'   => true,
            'output' => array('a'),
        ),
        array(
            'id' => 'li-global-page-background',
            'type' => 'background',
            'url' => true,
            'title' => __('Page Background Color/Image', 'leadinjection'),
            'subtitle' => __('Select the background color or an image for the page.', 'leadinjection'),
            'output' => array('body', '.js div#preloader'),
            'default'  => array(
                'background-color' => '#eeeeee',
            ),

        ),
        array(
            'id' => 'li-global-content-background',
            'type' => 'background',
            'url' => true,
            'title' => __('Content Wrapper Background Color/Image', 'leadinjection'),
            'subtitle' => __('Select the background color or an image for the content wrapper.', 'leadinjection'),
            'output' => array('.page-container'),
            'default'  => array(
                'background-color' => '#ffffff',
            ),

        ),
        array(
            'id' => 'li-global-nav-logo',
            'type' => 'media',
            'url' => true,
            'title' => __('Logo', 'leadinjection'),
            'subtitle' => __('Upload your logo.', 'leadinjection'),
            'default' => array(
                'url' => get_template_directory_uri() . '/img/nav-logo.png'
            ),
        ),
        array(
            'id' => 'li-global-favicon',
            'type' => 'media',
            'url' => true,
            'title' => __('Favicon Upload', 'leadinjection'),
            'subtitle' => __('Upload a 32px x 32px Png/Gif/Ico image that will represent your website favicon.', 'leadinjection'),
            'default' => array(
                'url' => get_template_directory_uri() . '/img/favicon.png'
            ),
        ),
        array(
            'id' => 'li-global-preloader',
            'type' => 'switch',
            'title' => __('Use Page Pre-Loader?', 'leadinjection'),
            'subtitle' => __('Display a loading animation until the page loads completely.', 'leadinjection'),
            'default' => true,
        ),
        array(
            'id' => 'li-global-scroll-top',
            'type' => 'switch',
            'title' => __('Display a scroll to top button?', 'leadinjection'),
            'subtitle' => __('Display a scroll to top button at the corner bottom right.', 'leadinjection'),
            'default' => true,
        ),
        array(
            'id' => 'li-global-raw-js-footer',
            'type' => 'textarea',
            'title' => __('Raw JS Footer', 'leadinjection'),
            'subtitle' => __('Output raw JavaScript in to your page footer.', 'leadinjection'),
        )
    )
));


Redux::setSection($opt_name, array(
    'title' => __('Top Bar Options', 'leadinjection'),
    'id' => 'global-topbar-options',
    'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-minus',
    'fields' => array(
        array(
            'id' => 'li-global-topbar',
            'type' => 'switch',
            'title' => __('Enable Top Bar', 'leadinjection'),
            'subtitle' => __('Display a Top Bar above the Navigation', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-topbar-background',
            'type' => 'background',
            'url' => true,
            'title' => __('Top Bar Background Color/Image', 'leadinjection'),
            'subtitle' => __('Select the background color or an image for the page.', 'leadinjection'),
            'output' => array('.li-topbar'),
            'required' => array('li-global-topbar', '=', true),
            'default'  => array(
                'background-color' => '#eeeeee',
            )
        ),
        array(
            'id' => 'li-global-topbar-social-icons',
            'type' => 'switch',
            'title' => __('Show Social icons', 'leadinjection'),
            'subtitle' => __('Display your Social icons in the Top Bar if they set in Socail Networks Tab.', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-topbar-address',
            'type' => 'switch',
            'title' => __('Show Address', 'leadinjection'),
            'subtitle' => __('Display your Address in the Top Bar', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-topbar-address-content',
            'type' => 'text',
            'title' => __('Top Bar Address', 'leadinjection'),
            'subtitle' => __('Enter an Address for the Top Bar', 'leadinjection'),
            'required' => array('li-global-topbar-address', '=', true)
        ),
        array(
            'id' => 'li-global-topbar-phone',
            'type' => 'switch',
            'title' => __('Show Phone Number', 'leadinjection'),
            'subtitle' => __('Display your Phone Number in the Top Bar', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-topbar-phone-content',
            'type' => 'text',
            'title' => __('Top Bar Phone Numner', 'leadinjection'),
            'subtitle' => __('Enter an Phone Number for the Topbar', 'leadinjection'),
            'required' => array('li-global-topbar-phone', '=', true)
        ),
        array(
            'id' => 'li-global-topbar-email',
            'type' => 'switch',
            'title' => __('Show Email', 'leadinjection'),
            'subtitle' => __('Display your Email Address in the Top Bar', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-topbar-email-content',
            'type' => 'text',
            'title' => __('Top Bar Email', 'leadinjection'),
            'subtitle' => __('Enter an Email-Address for the Top Bar', 'leadinjection'),
            'required' => array('li-global-topbar-email', '=', true)
        ),
    )
));


Redux::setSection($opt_name, array(
    'title' => __('Top Menu Options', 'leadinjection'),
    'id' => 'global-menu-options',
    'desc' => __('Default Theme Menu Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-compass',
    'fields' => array(
        array(
            'id' => 'li-global-nav-typography',
            'type' => 'typography',
            'title' => __('Typography', 'leadinjection'),
            'google' => true,
            'font-backup' => true,
            'output' => array('.header-nav .navbar ul li a'),
            'units' => 'px',
            'all_styles' => true,
            'subtitle' => __('Typography option with each property can be called individually.', 'leadinjection'),
        ),
        array(
            'id' => 'li-global-menue-bg-color',
            'type' => 'background',
            'title' => __('Top Menue Color/Image', 'leadinjection'),
            'subtitle' => __('Select a background color or an image for the top menu.', 'leadinjection'),
            'output' => array('.header-nav .navbar'),
        ),
        array(
            'id' => 'li-global-menue-border-color',
            'type' => 'color',
            'title' => __('Navbar Bottom Border Color', 'leadinjection'),
            'subtitle' => __('Select a background color or an image for the top menu.', 'leadinjection'),
            'output' => array('border-color' => '.header-nav .navbar'),
        ),
        array(
            'id'       => 'li-global-menu-link-color',
            'type'     => 'link_color',
            'title'    => __('Default Menu Link color', 'leadinjection'),
            'subtitle' => __('Select a default menu link color.', 'leadinjection'),
            'visited'   => true,
            'output' => array('.header-nav .navbar ul li a'),
        ),
        array(
            'id' => 'li-global-menue-active-item-color',
            'type' => 'color',
            'title' => __('Active Navbar Item color', 'leadinjection'),
            'subtitle' => __('Select a active item color for the top menu.', 'leadinjection'),
            'output' => array('.header-nav .navbar ul li.active a'),
        ),
        array(
            'id' => 'li-global-menue-dropdown-bg-color',
            'type' => 'background',
            'title' => __('Top Menue Dropdown Color/Image', 'leadinjection'),
            'subtitle' => __('Select a background color for the top menu dropdown.', 'leadinjection'),
            'output' => array('.header-nav .navbar ul li .dropdown-menu'),
        ),
        array(
            'id' => 'li-global-menue-dropdown-div-border-color',
            'type' => 'color',
            'title' => __('Top Menue Dropdown Divider Color', 'leadinjection'),
            'subtitle' => __('Select a Divider color for the top menu dropdown items.', 'leadinjection'),
            'output' => array('border-color' => '.header-nav .navbar ul li .dropdown-menu li'),
        ),
        array(
            'id'       => 'li-global-menu-dropdown-link-color',
            'type'     => 'link_color',
            'title'    => __('Default Dropdown Menu Link color', 'leadinjection'),
            'subtitle' => __('Select a default dropdown menu link color.', 'leadinjection'),
            'visited'   => true,
            'output' => array('.header-nav .navbar ul li .dropdown-menu li a'),
        ),
    )
));


Redux::setSection($opt_name, array(
    'title' => __('Blog Options', 'leadinjection'),
    'id' => 'global-blog-options',
    'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-adjust-alt',
    'fields' => array(
        array(
            'id' => 'li-global-blog-title',
            'type' => 'text',
            'title' => __('Blog / Category / Tag page Title', 'leadinjection'),
            'desc' => __('Leave blank to use the translation string.', 'leadinjection'),
            'subtitle' => __('Default page title for blog, category and tag.', 'leadinjection'),
        ),
        array(
            'id' => 'li-global-blog-header-bg-color',
            'type' => 'background',
            'title' => __('Blog Header Color/Image', 'leadinjection'),
            'subtitle' => __('Select a background color or an image for the blog header.', 'leadinjection'),
            'output' => array('.page-title'),
        ),
        array(
            'id' => 'li-global-blog-header-text-color',
            'type'     => 'color',
            'title'    => __('Blog Header Text Color', 'leadinjection'),
            'subtitle' => __('Select a text color for the blog header.', 'leadinjection'),
            'default'  => '#ffffff',
            'validate' => 'color',
            'output' => array('.page-title h1', '.page-title .breadcrumbs', '.page-title .breadcrumbs li a'),
        ),
    )
));


Redux::setSection($opt_name, array(
    'title' => __('Footer Options', 'leadinjection'),
    'id' => 'global-footer-options',
    'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-adjust-alt',
    'fields' => array(
        array(
            'id' => 'li-global-footer-widget-bar-color',
            'type' => 'background',
            'title' => __('Widget Bar Background Color/Image', 'leadinjection'),
            'subtitle' => __('Select the background color or an image for the footer widget bar.', 'leadinjection'),
            'output' => array('.site-footer .footer-widget-bar'),
        ),
        array(
            'id' => 'li-global-footer-widget-bar-content-color',
            'type' => 'color',
            'title' => __('Widget Bar Content Color', 'leadinjection'),
            'subtitle' => __('Select the content color for the footer widget bar.', 'leadinjection'),
            'output' => array('.site-footer .footer-widget-bar'),
        ),
        array(
            'id'       => 'li-global-footer-widget-bar-link-color',
            'type'     => 'link_color',
            'title'    => __('Default Footer Widget Bar Link color', 'leadinjection'),
            'subtitle' => __('Select a default footer widget bar link color.', 'leadinjection'),
            'visited'   => true,
            'output' => array('.footer-widget-bar a, .widget ul li a'),
        ),
        array(
            'id' => 'li-global-footer-copyright',
            'type' => 'textarea',
            'title' => __('Footer Copyright', 'leadinjection'),
            'subtitle' => __('Copyright to display in footer.', 'leadinjection'),
            'desc' => __("Enter a copyright information or anything you'd like.", 'leadinjection'),
            'default' => __('Copyright &copy; 2015 <a href="http://www.leadinjection.io">Leadinjection</a>. Powered by <a href="http://www.wordpress.org/">WordPress</a>.', 'leadinjection'),
        ),
        array(
            'id' => 'li-global-footer-copyright-color',
            'type' => 'background',
            'title' => __('Copyright Bar Color/Image', 'leadinjection'),
            'subtitle' => __('Select the background color or an image for the footer widget bar..', 'leadinjection'),
            'output' => array('.site-footer .footer-copyright'),
        ),
        array(
            'id' => 'li-global-footer-social-icons',
            'type' => 'switch',
            'title' => __('Show Social icons', 'leadinjection'),
            'subtitle' => __('Display your Social icons in the footer if they set in Socail Networks Tab.', 'leadinjection'),
            'default' => true,
        )
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Custom Button Colors', 'leadinjection'),
    'id' => 'global-button-colors',
    'desc' => __('Default Theme Options for leadinjection', 'leadinjection'),
    'icon' => 'el el-tint',
    'fields' => array(

        // Custom Button 1
        array(
            'id' => 'li-global-custom-button-1',
            'type' => 'switch',
            'title' => __('Enable Custom Button Style 1', 'leadinjection'),
            'subtitle' => __('Create a Custom Button Style ( Class: btn-custom1 )', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-custom-button-1-text',
            'type' => 'color',
            'title' => __('Custom Button 1 Text Color', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text.', 'leadinjection'),
            'output' => array('color' => '.btn-custom1, .btn-custom1.btn-outline'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-text-hover',
            'type' => 'color',
            'title' => __('Custom Button 1 Text Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text (hover).', 'leadinjection'),
            'output' => array('color' => '.btn-custom1:hover'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-background-nom',
            'type' => 'color',
            'title' => __('Custom Button 1 Background Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom1'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-background-hover',
            'type' => 'color',
            'title' => __('Custom Button 1 Background Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom1:hover, .btn-custom1.btn-outline:hover, .btn-custom1:focus'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-border-nom',
            'type' => 'color',
            'title' => __('Custom Button 1 Border Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom1, .btn-custom1.btn-outline'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-border-hover',
            'type' => 'color',
            'title' => __('Custom Button 1 Border Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom1:hover, .btn-custom1.btn-outline:hover'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-1-shadow',
            'type' => 'color',
            'title' => __('Custom Button 1 3D Shadow Color', 'leadinjection'),
            'subtitle' => __('Select the content color for the footer widget bar.', 'leadinjection'),
            'output' => array('border-bottom-color' => '.btn-custom1.btn-3d'),
            'required' => array('li-global-custom-button-1', '=', true)
        ),

        // Custom Button 2
        array(
            'id' => 'li-global-custom-button-2',
            'type' => 'switch',
            'title' => __('Enable Custom Button Style 2', 'leadinjection'),
            'subtitle' => __('Create a Custom Button Style ( Class: btn-custom2 )', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-custom-button-2-text',
            'type' => 'color',
            'title' => __('Custom Button 2 Text Color', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text.', 'leadinjection'),
            'output' => array('color' => '.btn-custom2, .btn-custom2.btn-outline'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-text-hover',
            'type' => 'color',
            'title' => __('Custom Button 2 Text Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text (hover).', 'leadinjection'),
            'output' => array('color' => '.btn-custom2:hover'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-background-nom',
            'type' => 'color',
            'title' => __('Custom Button 2 Background Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom2'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-background-hover',
            'type' => 'color',
            'title' => __('Custom Button 2 Background Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom2:hover, .btn-custom2.btn-outline:hover, .btn-custom2:focus'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-border-nom',
            'type' => 'color',
            'title' => __('Custom Button 2 Border Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom2, .btn-custom2.btn-outline'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-border-hover',
            'type' => 'color',
            'title' => __('Custom Button 2 Border Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom2:hover, .btn-custom2.btn-outline:hover'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-2-shadow',
            'type' => 'color',
            'title' => __('Custom Button 2 3D Shadow Color', 'leadinjection'),
            'subtitle' => __('Select the content color for the footer widget bar.', 'leadinjection'),
            'output' => array('border-bottom-color' => '.btn-custom2.btn-3d'),
            'required' => array('li-global-custom-button-2', '=', true)
        ),

        // Custom Button 3
        array(
            'id' => 'li-global-custom-button-3',
            'type' => 'switch',
            'title' => __('Enable Custom Button Style 3', 'leadinjection'),
            'subtitle' => __('Create a Custom Button Style ( Class: btn-custom3 )', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-custom-button-3-text',
            'type' => 'color',
            'title' => __('Custom Button 3 Text Color', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text.', 'leadinjection'),
            'output' => array('color' => '.btn-custom3, .btn-custom3.btn-outline'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-text-hover',
            'type' => 'color',
            'title' => __('Custom Button 3 Text Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text (hover).', 'leadinjection'),
            'output' => array('color' => '.btn-custom3:hover'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-background-nom',
            'type' => 'color',
            'title' => __('Custom Button 3 Background Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom3'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-background-hover',
            'type' => 'color',
            'title' => __('Custom Button 3 Background Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom3:hover, .btn-custom3.btn-outline:hover, .btn-custom3:focus'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-border-nom',
            'type' => 'color',
            'title' => __('Custom Button 3 Border Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom3, .btn-custom3.btn-outline'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-border-hover',
            'type' => 'color',
            'title' => __('Custom Button 3 Border Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom3:hover, .btn-custom3.btn-outline:hover'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-3-shadow',
            'type' => 'color',
            'title' => __('Custom Button 3 3D Shadow Color', 'leadinjection'),
            'subtitle' => __('Select the content color for the footer widget bar.', 'leadinjection'),
            'output' => array('border-bottom-color' => '.btn-custom3.btn-3d'),
            'required' => array('li-global-custom-button-3', '=', true)
        ),

        // Custom Button 4
        array(
            'id' => 'li-global-custom-button-4',
            'type' => 'switch',
            'title' => __('Enable Custom Button Style 4', 'leadinjection'),
            'subtitle' => __('Create a Custom Button Style ( Class: btn-custom4 )', 'leadinjection'),
            'default' => false,
        ),
        array(
            'id' => 'li-global-custom-button-4-text',
            'type' => 'color',
            'title' => __('Custom Button 4 Text Color', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text.', 'leadinjection'),
            'output' => array('color' => '.btn-custom4, .btn-custom4.btn-outline'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-text-hover',
            'type' => 'color',
            'title' => __('Custom Button 4 Text Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select color for the Button Text (hover).', 'leadinjection'),
            'output' => array('color' => '.btn-custom4:hover'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-background-nom',
            'type' => 'color',
            'title' => __('Custom Button 4 Background Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom4'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-background-hover',
            'type' => 'color',
            'title' => __('Custom Button 4 Background Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('background-color' => '.btn-custom4:hover, .btn-custom4.btn-outline:hover, .btn-custom1:focus'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-border-nom',
            'type' => 'color',
            'title' => __('Custom Button 4 Border Color', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom4, .btn-custom4.btn-outline'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-border-hover',
            'type' => 'color',
            'title' => __('Custom Button 4 Border Color (Hover)', 'leadinjection'),
            'subtitle' => __('Select a Button Background Color (hover)', 'leadinjection'),
            'output' => array('border-color' => '.btn-custom4:hover, .btn-custom4.btn-outline:hover'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
        array(
            'id' => 'li-global-custom-button-4-shadow',
            'type' => 'color',
            'title' => __('Custom Button 4 3D Shadow Color', 'leadinjection'),
            'subtitle' => __('Select the content color for the footer widget bar.', 'leadinjection'),
            'output' => array('border-bottom-color' => '.btn-custom4.btn-3d'),
            'required' => array('li-global-custom-button-4', '=', true)
        ),
    )
));



Redux::setSection($opt_name, array(
    'title' => __('API Keys', 'leadinjection'),
    'id' => 'li-global-api-keys',
    'icon' => 'el el-key',
    'fields' => array(
        array(
            'id'        => 'li-global-api-key-gmaps',
            'type'      => 'text',
            'title'     => __('Google Maps API Key', 'leadinjection'),
            'subtitle'  => __('Please enter your Google Maps API Key', 'leadinjection'),
            'desc'      => __('You can create a new Google Api Key here: <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">https://developers.google.com/maps/documentation/javascript/get-api-key</a>', 'leadinjection'),
            'default'   => '',
        )
    )
));



Redux::setSection($opt_name, array(
    'title' => __('Social Networks', 'leadinjection'),
    'id' => 'social_networks',
    'icon' => 'el el-network',
    'fields' => array(
        array(
            'id'        => 'sn_facebook_url',
            'type'      => 'text',
            'title'     => __('Facebook URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Facebook URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_twitter_url',
            'type'      => 'text',
            'title'     => __('Twitter URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Twitter URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_googleplus_url',
            'type'      => 'text',
            'title'     => __('Google+ URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Google+ URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_youtube_url',
            'type'      => 'text',
            'title'     => __('Youtube URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Youtube URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_vimeo_url',
            'type'      => 'text',
            'title'     => __('Vimeo URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Vimeo URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_linkedin_url',
            'type'      => 'text',
            'title'     => __('Linkedin URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Linkedin URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_tumblr_url',
            'type'      => 'text',
            'title'     => __('Tumblr URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Tumblr URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_pinterest_url',
            'type'      => 'text',
            'title'     => __('Pinterest URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Pinterest URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_skype_url',
            'type'      => 'text',
            'title'     => __('Skype ID', 'leadinjection'),
            'subtitle'  => __('Enter your skype ID.', 'leadinjection'),
            
            'default'   => '',

        ),
        array(
            'id'        => 'sn_dribbble_url',
            'type'      => 'text',
            'title'     => __('Dribbble URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Dribbble URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_behance_url',
            'type'      => 'text',
            'title'     => __('Behance URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Behance URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_flickr_url',
            'type'      => 'text',
            'title'     => __('Flickr URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Flickr URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_instagram_url',
            'type'      => 'text',
            'title'     => __('Instagram URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Instagram URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_deviantart_url',
            'type'      => 'text',
            'title'     => __('Deviant Art URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Deviant Art URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_digg_url',
            'type'      => 'text',
            'title'     => __('Digg URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Digg URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        ),
        array(
            'id'        => 'sn_reddit_url',
            'type'      => 'text',
            'title'     => __('Reddit URL', 'leadinjection'),
            'subtitle'  => __('Please enter in your Reddit URL.', 'leadinjection'),

            'validate'  => 'url',
            'default'   => '',

        )
    )
));

Redux::setSection($opt_name, array(
    'title' => __('Custom CSS', 'leadinjection'),
    'id' => 'global-css-code',
    'icon' => 'el el-css',
    'fields' => array(
        array(
            'id' => 'li-global-css-code',
            'type' => 'textarea',
            'title' => __('Custom CSS Code', 'leadinjection'),
            'subtitle' => __('Enter Custom CSS Code', 'leadinjection'),
            'rows' => 30,
            'desc' => __("Enter custom CSS (Note: it will be outputted on each page).", 'leadinjection'),
        ),

    )
));
