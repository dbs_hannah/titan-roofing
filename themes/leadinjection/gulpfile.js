var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var livereload = require('gulp-livereload');
var gsgc = require('gulp-sass-generate-contents');
var cssbeautify = require('gulp-cssbeautify');
var clean = require('gulp-clean');


var creds = {
    "Theme Name":   "Leadinjection",
    "Theme URI":  "http://leadinjection.io/",
    "Author":  "http://themeinjection.com/",
    "Author URI":  "http://themeinjection.com/",
    "Description":  "Leadinjection was designed for professional marketeers, business owners and affiliates to launch landing pages within minutes.",
    "Version:":  "1.1.8",
    "License":  "GNU General Public License v2 or later",
    "License URI":  "http://www.gnu.org/licenses/gpl-2.0.html",
    "Text Domain":  "leadinjection",
    "Tags":  "one-column, two-columns, right-sidebar, custom-header, custom-menu, editor-style, featured-images, translation-ready",
}

var plumberErrorHandler = { errorHandler: notify.onError({
    title: 'Gulp',
    message: 'Error: <%= error.message %>'
  })
};

 
// Sass
gulp.task('sass', function () {
    gulp.src('./css/*.scss')
    	.pipe(plumber(plumberErrorHandler))
        .pipe(sass())
        .pipe(gulp.dest('./css/'))
        .pipe(livereload());
});

//sass-generate-contents
gulp.task('sass-generate-toc', function () {
    gulp.src('css/src/*.scss')
        .pipe(gsgc('css/style.scss', creds))
        .pipe(gulp.dest('css'));
});

// Css Beautify
gulp.task('css-beautify', function() {
    return gulp.src('./css/style.css')
        .pipe(cssbeautify())
        .pipe(gulp.dest('./'));
});

// JS Hint & Concat
gulp.task('js', function () {
	gulp.src('js/src/*.js')
		.pipe(plumber(plumberErrorHandler))
		.pipe(jshint())
		.pipe(jshint.reporter('fail'))
		.pipe(concat('theme.js'))
		.pipe(gulp.dest('js'))
		.pipe(livereload());
});


gulp.task('copy-build', function(){
    gulp.src(['./**/*']).pipe(gulp.dest('../../../../leadinjection-theme-build'));
});


gulp.task('clean-build', function () {
    gulp.src([
        '../../../../leadinjection-theme-build/node_modules/',
        '../../../../leadinjection-theme-build/**/.git',
        '../../../../leadinjection-theme-build/**/.gitignore',
        '../../../../leadinjection-theme-build/**/.DS_Store',
        '../../../../leadinjection-theme-build/**/.bower.json',
        '../../../../leadinjection-theme-build/**/.jshintrc',
        '../../../../leadinjection-theme-build/**/.jscsrc',
        '../../../../leadinjection-theme-build/**/.csscomb.json',
        '../../../../leadinjection-theme-build/**/.csslintrc',
        '../../../../leadinjection-theme-build/**/.npmignore',
        '../../../../leadinjection-theme-build/**/.eslintrc',
        '../../../../leadinjection-theme-build/**/.travis.yml'],
        {read: false})
        .pipe(clean({force: true}));
});



// Watch
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('css/src/*.scss', ['sass-generate-toc']);
  gulp.watch('css/*.scss', ['sass']);
  gulp.watch('css/style.css', ['css-beautify']);
  gulp.watch('js/src/*.js', ['js']);
});
 
gulp.task('default', ['sass-generate-toc', 'sass', 'js', 'css-beautify', 'watch']);



gulp.task('theme-build', ['sass-generate-toc', 'sass', 'js', 'css-beautify', 'copy-build']);



// gulp.task('default', function(){
 
//     console.log('default gulp task...')
 
// });