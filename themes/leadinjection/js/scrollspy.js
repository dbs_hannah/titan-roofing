(function ($) {

    "use strict";

    // Navbar Scrollspy Fix
    // -------------------------------------------------------------------------------

    $('.navbar a').each( function() {
        if (this.hash.indexOf('#') === 0) {
            // Alert this to an absolute link (pointing to the correct section on the hompage)
            this.href = this.hash;
        }
    });



})(jQuery);


