(function ($) {

    "use strict";


    // Site preloader
    // -------------------------------------------------------------------------------
    $(window).load(function () {
        $('#preloader').fadeOut('slow', function () {
            $(this).remove();
        });
    });


    // Scrollspy
    //-------------------------------------------------------------------------------
    $('body').scrollspy({target: '.navbar', offset: 100});

   
    // Scroll To Animation
    //-------------------------------------------------------------------------------
    var scrollTo = $(".scroll-to");

    scrollTo.on('click', function (event) {
        $('.modal').modal('hide');
        var position = $(document).scrollTop();
        var scrollOffset = 99;

        var marker = $(this).attr('href');

        if(typeof marker === 'undefined'){
            marker = $(this).find('a').attr('href');
        };

        marker = marker.substr(marker.indexOf('#'));

        //console.log(marker);

        $('html, body').animate({scrollTop: $(marker).offset().top - scrollOffset}, 'slow');
        return false;
    });

    // Scroll Up Btn
    //-------------------------------------------------------------------------------
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scroll-up-btn').removeClass("animated fadeOutRight");
            $('.scroll-up-btn').fadeIn().addClass("animated fadeInRight");
        } else {
            $('.scroll-up-btn').removeClass("animated fadeInRight");
            $('.scroll-up-btn').fadeOut().addClass("animated fadeOutRight");
        }
    });


    // Open a Modal from Nav
    //-------------------------------------------------------------------------------
    var modalLink = $(".liModal");

    modalLink.on('click', function (event) {
        var modalId = $(this).children("a").attr('href');
        console.log(modalId);
        $(modalId).modal();
        return false;
    });


    // Navigation Top
    // -------------------------------------------------------------------------------
    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 300) {
            $('.navbar-hidden').fadeIn();
        } else {
            $('.navbar-hidden').fadeOut();
        }
    });

    // Top Widget Bar
    // -------------------------------------------------------------------------------
    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 20) {
            $('.navbar-widgets-fade').fadeOut();
        } else {
            $('.navbar-widgets-fade').fadeIn();
        }
    });



    // Animation
    // -------------------------------------------------------------------------------
    $(window).load(function () {  // run after all scripts a loaded

        if (jQuery().waypoint) {     // check if waypoint function exists!

            $('.li-animate').waypoint(function () {

                var effect = $(this).data('effect');
                $(this).css('visibility', 'visible');
                $(this).addClass('animated');
                $(this).addClass(effect);

            }, {
                offset: '87%',
                triggerOnce: true
            });
        }

    });

    // Number Counter
    // -------------------------------------------------------------------------------
    $(window).load(function () {  // run after all scripts a loaded

        if (jQuery().waypoint && jQuery().animateNumber) {     // check if waypoint function exists!

            $('.number-counter-value').waypoint(function () {

                var numberValueStart = $(this).data('start');
                var numberValueEnd = $(this).data('end');

                var numberValueSpeed = $(this).data('speed');
                //console.log(numberValueSpeed);
                if (undefined === numberValueSpeed) {
                    numberValueSpeed = 5000;
                }
                $(this).prop('number', numberValueStart).animateNumber({number: numberValueEnd}, numberValueSpeed);

            }, {
                offset: '87%',
                triggerOnce: true
            });
        }

    });

    // Pricing Table Simple
    // -------------------------------------------------------------------------------
    $(".pricing-table-col").on("mouseover", function () {

        var animation = 'swing';

        $(".pricing-table-col").removeClass("highlight");
        $(".pricing-table-col .action").removeClass("animated "+animation);
        $(this).addClass("highlight")
        $(this).find(".action").addClass("animated "+animation);
    });


    // Accordion
    // -------------------------------------------------------------------------------
    $(".li-accordion .panel-title a").on("click", function(){

        $(".li-accordion .panel-title a i").removeClass("fa-dot-circle-o");
        $(".li-accordion .panel-title a i").addClass("fa-circle-o");

        $("i", this).removeClass("fa-circle-o");
        $("i", this).addClass("fa-dot-circle-o");
    });



})(jQuery);


// Rev Slider Modal Open CallBack
// -----------------------------------------------------------------------------------
function openModal(liModalId){
    jQuery('#'+liModalId).modal();
}


