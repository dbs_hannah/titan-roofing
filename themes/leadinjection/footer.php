<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leadinjection
 *
 *
 */

// leadinjection Global options
$leadinjection_global_option = get_option( 'rdx_option' );

if(is_home()){
    $post_id = get_option( 'page_for_posts' );
}else{
    $post_id = get_the_ID();
}

// leadinjection OnPage options
$leadinjection_onpage_footer = get_post_meta( $post_id, 'li-onpage-footer', true );
$leadinjection_onpage_footer_copyright = get_post_meta( $post_id, 'li-onpage-footer-copyright', true );
$leadinjection_onpage_footer_social_icons = get_post_meta( $post_id, 'li-onpage-footer-social-icons', true );
$leadinjection_onpage_raw_js_footer = get_post_meta( $post_id, 'li-onpage-raw-js-footer', true );
$leadinjection_onpage_scroll_top = get_post_meta( $post_id, 'li-onpage-scroll-top', true );

?>

</div><!-- #content -->

<?php if (empty($leadinjection_onpage_footer) || 'hidden' !== $leadinjection_onpage_footer) : ?>

    <footer id="colophon" class="site-footer">

        <?php if (is_active_sidebar('footer-widget-bar-col1') || is_active_sidebar('footer-widget-bar-col2') || is_active_sidebar('footer-widget-bar-col3')) : ?>
            <div class="footer-widget-bar footer-row">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-widget">
                                <?php if (is_active_sidebar('footer-widget-bar-col1')) {
                                    if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 1 column')) : ?>
                                    <?php endif;
                                } ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-widget">
                                <?php if (is_active_sidebar('footer-widget-bar-col2')) {
                                    if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 2 column')) : ?>
                                    <?php endif;
                                } ?>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-widget">
                                <?php if (is_active_sidebar('footer-widget-bar-col3')) {
                                    if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer 3 column')) : ?>
                                    <?php endif;
                                } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>

        <div class="footer-copyright footer-row">
            <div class="container">
                <div class="row">

                    <?php if ( !empty($leadinjection_global_option['li-global-footer-copyright']) && false == $leadinjection_onpage_footer_copyright ) : ?>
                        <div class="col-sm-8">
                            <div class="site-info">
                                <?php echo $leadinjection_global_option['li-global-footer-copyright']; ?>
                            </div>
                            <!-- .site-info -->
                        </div>
                    <?php elseif (!empty($leadinjection_onpage_footer_copyright)) : ?>
                        <div class="col-sm-8">
                            <div class="site-info">
                                <?php echo $leadinjection_onpage_footer_copyright; ?>
                            </div>
                            <!-- .site-info -->
                        </div>
                    <?php endif; ?>


                    <?php if ( !empty($leadinjection_onpage_footer_social_icons) && !empty($leadinjection_global_option['li-global-footer-social-icons']) ) : ?>
                        <div class="col-sm-4">
                            <div class="footer-social-icons">
                                <?php esc_html(leadinjection_global_social_icons()); ?>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </footer><!-- #colophon -->
<?php endif; ?>

</div><!-- #page -->

</div><!-- #page-container -->


<?php if(!empty($leadinjection_global_option['li-global-scroll-top']) && empty($leadinjection_onpage_scroll_top) || !empty($leadinjection_onpage_scroll_top) ) : ?>
<a href="#li-page-top" class="scroll-to scroll-up-btn hidden-xs"><i class="fa fa-angle-double-up"></i></a>
<?php endif; ?>


<?php wp_footer(); ?>

<?php
if (!empty($leadinjection_global_option['li-global-raw-js-footer']) && empty($leadinjection_onpage_raw_js_footer)) {

    echo $leadinjection_global_option['li-global-raw-js-footer'];

}elseif( !empty($leadinjection_onpage_raw_js_footer) ){

    echo $leadinjection_onpage_raw_js_footer;

}
?>


<!-- Leadinjection v1.1.8 -->

</body>
</html>

