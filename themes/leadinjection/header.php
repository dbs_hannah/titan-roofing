<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package leadinjection
 */

// leadinjection Global options
$leadinjection_global_option = get_option( 'rdx_option' );

// Check if Blog Home page
if(is_home()){
    $post_id = get_option( 'page_for_posts' );
}else{
    $post_id = get_the_ID();
}

// leadinjection OnPage options
$leadinjection_onpage_favicon = get_post_meta( $post_id, 'li-onpage-favicon', true );
$leadinjection_onpage_raw_js_head = get_post_meta( $post_id, 'li-onpage-raw-js-head', true );
$leadinjection_onpage_preloader = get_post_meta( $post_id, 'li-onpage-preloader', true );
$leadinjection_onpage_header_nav = get_post_meta( $post_id, 'li-onpage-header-nav', true );
$leadinjection_onpage_nav_logo = get_post_meta( $post_id, 'li-onpage-nav-logo', true );
$leadinjection_onpage_display_mode = get_post_meta( $post_id, 'li-onpage-display-mode', true );



?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="js">
<head>
    <?php if(!empty( $leadinjection_onpage_raw_js_head )){
        echo $leadinjection_onpage_raw_js_head;
    } ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

    <?php wp_head(); ?>

    <?php

    // Outputs leadinjection favicon code only if the user has not set a WP site icon
    // or if they are on versions of WordPress older than 4.3

    if ( ! ( function_exists( 'has_site_icon' ) && has_site_icon() ) ) : ?>

        <?php if (!empty($leadinjection_global_option['li-global-favicon']['url']) && empty($leadinjection_onpage_favicon['url'])) : ?>
            <link rel="shortcut icon" href="<?php echo esc_url($leadinjection_global_option['li-global-favicon']['url']); ?>"/>
        <?php elseif (!empty($leadinjection_onpage_favicon['url'])) : ?>
            <link rel="shortcut icon" href="<?php echo esc_url($leadinjection_onpage_favicon['url']); ?>"/>
        <?php endif; ?>

    <?php endif; ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <?php do_action('leadinjection_ie8_support'); ?>
    <![endif]-->

    <?php if ( is_admin_bar_showing()) : ?>
        <style>
            .navbar-fixed-top{top:32px;}
            .vc_editor .navbar-fixed-top{top:0px !important;}
        </style>
    <?php endif; ?>


    <?php
    if (!empty($leadinjection_global_option['li-global-css-code'])) {
        echo "<style>";
        echo $leadinjection_global_option['li-global-css-code'];
        echo "</style>";
    }
    ?>

</head>

<body <?php body_class(); ?>>
<?php if(!empty($leadinjection_global_option['li-global-preloader']) && empty($leadinjection_onpage_preloader) || !empty($leadinjection_onpage_preloader) ) : ?>
    <div id="preloader">
        <div class="loader">Loading...</div>
    </div>
<?php endif; ?>


<div id="li-page-top" class="page-container <?php if(!empty($leadinjection_onpage_display_mode)){ echo $leadinjection_onpage_display_mode; }else{ echo 'fluid'; } ?>
<?php if(!empty($leadinjection_onpage_header_nav) && 'fixed' == $leadinjection_onpage_header_nav ) : ?> fixed-nav<?php endif; ?> ">


    <!-- start header nav -->
    <?php if(empty($leadinjection_onpage_header_nav) || 'hidden' !== $leadinjection_onpage_header_nav) : ?>

        <div class="header-nav">
                <nav class="navbar navbar-default
                    <?php if( 'fixed' == $leadinjection_onpage_header_nav || 'fade-in' == $leadinjection_onpage_header_nav ) : ?>navbar-fixed-top <?php endif; ?>
                    <?php if( 'fade-in' == $leadinjection_onpage_header_nav ) : ?>navbar-hidden <?php endif; ?>">



                    <?php if( !empty($leadinjection_global_option['li-global-topbar']) ) : ?>
                    <div class="li-topbar hidden-xs <?php if( 'fixed' == $leadinjection_onpage_header_nav ) : ?>navbar-widgets-fade<?php endif; ?>">
                        <div class="container fluid-on-sm">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if( !empty($leadinjection_global_option['li-global-topbar-social-icons']) ) : ?>
                                    <div class="social-icons">
                                        <?php leadinjection_global_social_icons(); ?>
                                    </div>
                                    <?php endif; ?>


                                    <?php if( !empty($leadinjection_global_option['li-global-topbar-address']) ||
                                              !empty($leadinjection_global_option['li-global-topbar-phone']) ||
                                              !empty($leadinjection_global_option['li-global-topbar-email']) ) : ?>
                                    <div class="contact-info">
                                        <?php if(!empty($leadinjection_global_option['li-global-topbar-address'])) : ?>
                                            <?php $topbar_address = ( !empty($leadinjection_global_option['li-global-topbar-address']) ) ? $leadinjection_global_option['li-global-topbar-address-content'] : ''; ?>
                                            <a class="address" href="https://www.google.com/maps/dir/Current+Location/<?php echo urlencode($topbar_address); ?>">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $topbar_address; ?>
                                            </a>
                                        <?php endif; ?>

                                        <?php if(!empty($leadinjection_global_option['li-global-topbar-phone'])) : ?>
                                            <?php $topbar_phone = ( !empty($leadinjection_global_option['li-global-topbar-phone']) ) ? $leadinjection_global_option['li-global-topbar-phone-content'] : ''; ?>
                                            <a class="phone" href="tel://<?php echo $topbar_phone; ?>">
                                                <i class="fa fa-comments-o" aria-hidden="true"></i> <?php echo $topbar_phone; ?>
                                            </a>
                                        <?php endif; ?>

                                        <?php if(!empty($leadinjection_global_option['li-global-topbar-email'])) : ?>
                                            <?php $topbar_email = ( !empty($leadinjection_global_option['li-global-topbar-email']) ) ? $leadinjection_global_option['li-global-topbar-email-content'] : ''; ?>
                                            <a class="email" href="mailto:<?php echo $topbar_email; ?>">
                                                <i class="fa fa-envelope-o" aria-hidden="true"></i> <?php echo $topbar_email; ?>
                                            </a>
                                        <?php endif; ?>
                                    </div>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>


                    <div class="container fluid-on-sm">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                    data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar top-bar"></span>
                                <span class="icon-bar middle-bar"></span>
                                <span class="icon-bar bottom-bar"></span>
                            </button>
                            <div class="navbar-brand-container">
                            <a class="navbar-brand" href="<?php echo home_url(); ?>">
                                <?php if (!empty($leadinjection_global_option['li-global-nav-logo']['url']) && empty($leadinjection_onpage_nav_logo['url'])) : ?>
                                    <img src="<?php echo $leadinjection_global_option['li-global-nav-logo']['url']; ?>" alt="<?php bloginfo('name'); ?>">
                                <?php elseif(!empty($leadinjection_onpage_nav_logo['url'])) : ?>
                                    <img src="<?php echo $leadinjection_onpage_nav_logo['url']; ?>" alt="<?php bloginfo('name'); ?>">
                                <?php else : ?>
                                    <?php bloginfo('name'); ?>
                                <?php endif; ?>
                            </a>
                            </div>
                        </div>

                        <?php
                        wp_nav_menu(array(
                                'menu' => 'primary',
                                'theme_location' => 'primary',
                                'depth' => 2,
                                'container' => 'div',
                                'container_class' => 'collapse navbar-collapse',
                                'container_id' => 'bs-example-navbar-collapse-1',
                                'menu_class' => 'nav navbar-nav navbar-right',
                                'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                                'walker' => new wp_bootstrap_navwalker())
                        );
                        ?>
                    </div>
                </nav>

        </div>

    <?php endif; ?>
    <!-- end header nav -->


    <div id="page" class="hfeed site">
        <a class="skip-link screen-reader-text" href="#content"><?php esc_html_e('Skip to content', 'leadinjection'); ?></a>



        <div id="content" class="site-content">
